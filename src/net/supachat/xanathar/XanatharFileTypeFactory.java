package net.supachat.xanathar;

import com.intellij.openapi.fileTypes.*;
import org.jetbrains.annotations.NotNull;


public class XanatharFileTypeFactory extends FileTypeFactory {
    @Override
    public void createFileTypes(@NotNull FileTypeConsumer fileTypeConsumer) {
        fileTypeConsumer.consume(XanatharFileType.INSTANCE);
    }
}