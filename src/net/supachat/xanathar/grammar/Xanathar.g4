grammar Xanathar;

COMMENT: '#' ~( '\r' | '\n' )* -> channel(HIDDEN);

prep : '!=!' .+? '!=!';
DIGIT : [0-9];

USE : 'use' ;
DECLARE : 'declare' ;
LOCKED : 'locked' ;
MUT : 'mut' ;
LET : 'let' ;
STATIC : 'static' ;
SEALED : 'sealed' ;
EXTENDS : 'extends' ;
INTERFACE : 'interface' ;
FN : 'fn' ;
HOOK : 'hook' ;
TEMPLATE : 'template' ;
CLASS : 'class' ;
VIRTUAL : 'virtual' ;
AS : 'as' ;
OVERRIDE : 'override' ;
IF : 'if' ;
ELSE : 'else' ;
IS : 'is' ;
FOR : 'for' ;
WHILE : 'while' ;
RET : 'ret' ;
MATCH : 'match' ;
DEFAULT : 'default' ;
BREAK : 'break' ;
SKIP_ : 'SKIP_' ;
LOCK : 'lock' ;
UNLOCK : 'unlock' ;
SIZEOF : 'sizeof' ;
TYPEOF : 'typeof' ;
NEW : 'new' ;

ARROW : '->';
PLUS : '+';
MINUS : '-';
TIMES : '*';
DIV : '/';
MOD : '%';
ROL : '<<';
ROR : '>>';
LAMBDA : 'λ';
LEFT_ANGLE : '<';
RIGHT_ANGLE : '>';
GEQ : '<=';
LEQ : '>=';
DOT : '.';
AMPERSAND : '&';
PIPE : '|';
CARET : '^';

PARAMS : ( VARIABLE_PHRASE AS_PHRASE ',' )* (VARIABLE_PHRASE AS_PHRASE);
VARIABLE_PHRASE : '$' WORD;
AS_PHRASE : AS (TYPESPEC | WORD) ('[' NUMBER ']')*;
WORD: ('_' | LETTER) ('_' | LETTER | DIGIT)*;


//%ignore PREPROCESS;
cast : '[' expression AS_PHRASE ']';

USE_STATEMENT : USE WORD ;
declare_statement : DECLARE VARIABLE_PHRASE AS_PHRASE ('(' expression ')')? ;
//modifiers : (LOCKED | MUT)*;
RAW_VAR : '@' WORD;
TYPESPEC : (TIMES | AMPERSAND)+ WORD;
variable_setting : (VARIABLE_PHRASE | REF) ('(' expression ')')? '=' (expression|fncall) ;
let : LET VARIABLE_PHRASE '=' expression ;

class_ : class__specs 'class_' WORD LEFT_ANGLE types RIGHT_ANGLE inheritance (':' inheritance)? '{' (phrase*) '}';
class__specs : (STATIC | SEALED) *;
extends_ : EXTENDS WORD '{' (phrase*) '}';

INTERFACE_ : INTERFACE WORD '{' (INTERFACE_FN)* '}';
INTERFACE_FN : FN WORD '[' (TYPESPEC ',')* (TYPESPEC)? ']' AS_PHRASE ;
DECLARED_VAR : VARIABLE_PHRASE AS_PHRASE;
types : ((DECLARED_VAR',')* DECLARED_VAR)?;
inheritance : (('(' (WORD ',')* WORD ')'))?;

// fncall : varspec '[' (expression ',')* [expression] ']';
//spec : (raw_var | variable_phrase | ref | expression) DOT word;
fncall : expression '[' (expression ',')* (expression)? ']';
//generic_call : expression LEFT_ANGLE gp RIGHT_ANGLE '[' (expression ',')* (expression)? ']';
GP : ((TYPESPEC|WORD) ',')* (TYPESPEC|WORD);
fn : FN WORD '[' PARAMS? ']' AS_PHRASE '{' ( phrase )* '}';
hook : HOOK OP '[' PARAMS ']' (AS_PHRASE)? '{' ( phrase )* '}';
OP : PLUS | MINUS | TIMES | DIV | MOD | PIPE | AMPERSAND;
template_ : TEMPLATE LEFT_ANGLE CLASSES RIGHT_ANGLE WORD '[' PARAMS ']' AS_PHRASE '{' (phrase)* '}';
CLASSES : (CLASS_ ',')* CLASS_;
CLASS_ : CLASS WORD;
VIRTUAL_FN : VIRTUAL WORD '[' (TYPESPEC ',')* (TYPESPEC)? ']' AS (TYPESPEC|WORD) ;
override : OVERRIDE '@' OVERRIDE_NAME LEFT_ANGLE override_param RIGHT_ANGLE '{' (phrase)* '}';
OVERRIDE_NAME : (WORD ARROW)+ WORD;
override_param : (VARIABLE_PHRASE ',')* (VARIABLE_PHRASE)? ;

lambda : LAMBDA PARAMS ARROW (expression|('{' (phrase)* '}')) ;

// if_ : 'if_' '[' ( expression ) ']' '{' ( phrase )* '}' ('else_' if_)* ['else_' '{' ( phrase )* '}'];
if_ : IF '[' ( expression ) ']' '{' ( phrase )* '}' (else_)?;
else_ : ELSE '{' ( phrase )* '}';
// for_ : 'for_' '[' variable_phrase IS expression ']' '{' (phrase)* '}';
for_ : FOR '[' phrase expression ';' phrase ']' '{' (phrase)* '}';
while_ : WHILE '[' expression ']' '{' (phrase)* '}';
ret : RET (expression)? ;
match_ : MATCH '[' expression ']' '{' (match_arm ',')* (match_arm|default_arm)? ','? '}';
match_arm : expression '=>' phrase;
default_arm : DEFAULT '=>' phrase;

REF : (VARIABLE_PHRASE|RAW_VAR) (ARROW (VARIABLE_PHRASE|RAW_VAR))+;

ptr : AMPERSAND expression;
deref : TIMES expression;

NUMBER : (PLUS | MINUS)? DIGIT+;
FLOAT : NUMBER DOT NUMBER;

new_ : 'new_' expression AS fncall;

//call_sealed_class_ : (word '::')+ fncall;

lock : LOCK VARIABLE_PHRASE;
unlock : UNLOCK VARIABLE_PHRASE;

inc : expression '++';
dec : expression '--';

reg : MOD WORD;
reg_set : reg '<-' expression;

expression :
        WORD                                                                                    # Bareword
        | ('(' expression ')')                                                                  # Parentheses
        | reg                                                                                   # GetRegister
        | expression '++'                                                                       # Increment
        | expression '--'                                                                       # Decrement
        | SIZEOF '@' (TYPESPEC|WORD)                                                          # GetSizeOf
        | TYPEOF '@' expression                                                               # GetTypeOf
        | REF                                                                                   # Reference
        | cast                                                                                  # Casting
        | ptr                                                                                   # Pointer
        | expression '(' expression ')'                                                         # ArrayIndex
        | deref                                                                                 # Dereference
        | NUMBER                                                                                # NumberConst
        | FLOAT                                                                                # FloatConst
        | STR                                                                                   # String
        | VARIABLE_PHRASE                                                                       # VariablePhrase
        | RAW_VAR                                                                               # RawVariable
        | expression PLUS expression                                                             # Addition
        | expression MINUS expression                                                             # Subtraction
        | expression TIMES expression                                                             # Multiplication
        | expression DIV expression                                                             # Division
        | expression AMPERSAND expression                                                             # BinaryAnd
        | expression PIPE expression                                                             # BinaryOr
        | expression CARET expression                                                             # BinaryXor
        | expression MOD expression                                                             # Modulo
        | expression '[' (expression ',')* (expression)? ']'                                    # FunctionCall
        | expression LEFT_ANGLE GP RIGHT_ANGLE '[' (expression ',')* (expression)? ']'                         # GenericCall
        | LAMBDA PARAMS ARROW (expression|('{' (phrase)* '}'))                                      # LambdaCreation
        | (WORD '::')+ WORD '[' (expression ',')* (expression)? ']'                             # CallSealedClass
        | expression '..' expression                                                            # Range
        | expression IS expression                                                            # Is
        | expression ROL expression                                                            # RotateLeft
        | expression ROR expression                                                            # RotateRight
        | expression LEFT_ANGLE expression                                                             # Less
        | expression RIGHT_ANGLE expression                                                             # Greater
        | expression GEQ expression                                                            # LessOrEqual
        | expression LEQ expression                                                            # GreaterOrEqual
        | expression '!=' expression                                                            # NotEqual
        | expression '==' expression                                                            # Equal
        | expression DOT (WORD DOT)* WORD                                                       # Spec
        | '!' expression                                                                        # BinaryNot
        | NEW expression AS expression '[' (expression ',')* (expression)? ']'              # NewCall
        ;
phrase : ((expression
            | reg_set
            | prep
            | inc
            | dec
            | asm_
            | USE_STATEMENT
            | declare_statement
            | template_
            | lambda
//            | as_phrase
            | ret
            | lock
            | unlock
            | variable_setting
            | let
            | BREAK
            | SKIP_
            | new_
            ) ';') | (
            if_
            | fn
            | hook
            | VIRTUAL_FN
            | match_
            | while_
            | for_
            | class_
            | INTERFACE_
            | override
            | extends_
            );
start: phrase*;

asm_ : '__asm__' .+? '__end-asm__';

fragment STRING_INNER : ~'"'|'\\' ;
fragment ESCAPED_STRING : '"' STRING_INNER* '"';
fragment LETTER: [a-zA-Z];
STR : ESCAPED_STRING;

WS : [ \r\t\n]+ -> channel(HIDDEN);

/** "catch all" rule for any char not matche in a token rule of your
 *  grammar. Lexers in Intellij must return all tokens good and bad.
 *  There must be a token to cover all characters, which makes sense, for
 *  an IDE. The parser however should not see these bad tokens because
 *  it just confuses the issue. Hence, the hidden channel.

 * Copied from https://github.com/antlr/jetbrains-plugin-sample/blob/master/src/grammars/org/antlr/jetbrains/sample/parser/SampleLanguage.g4
 */
ERRCHAR
	:	.	-> channel(HIDDEN)
	;