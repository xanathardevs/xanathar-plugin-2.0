package net.supachat.xanathar;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

public class XanatharIcons {
    public static final Icon FILE = IconLoader.getIcon("/net/supachat/xanathar/icons/icon.png");
}