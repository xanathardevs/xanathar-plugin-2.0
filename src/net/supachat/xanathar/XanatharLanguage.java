package net.supachat.xanathar;

import com.intellij.lang.Language;

public class XanatharLanguage extends Language {
    public static final XanatharLanguage INSTANCE = new XanatharLanguage();

    private XanatharLanguage() {
        super("Xanathar");
    }
}
