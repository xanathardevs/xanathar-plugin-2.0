package net.supachat.xanathar.psi;

import com.intellij.psi.tree.IElementType;
import net.supachat.xanathar.XanatharLanguage;
import org.jetbrains.annotations.*;

public class XanatharTokenType extends IElementType {
    public XanatharTokenType(@NotNull @NonNls String debugName) {
        super(debugName, XanatharLanguage.INSTANCE);
    }

    @Override
    public String toString() {
        return "XanatharTokenType." + super.toString();
    }
}
