package net.supachat.xanathar.psi;

import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.psi.FileViewProvider;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiNamedElement;
import org.antlr.intellij.adaptor.SymtabUtils;
import org.antlr.intellij.adaptor.psi.ScopeNode;
import net.supachat.xanathar.XanatharIcons;
import net.supachat.xanathar.XanatharFileType;
import net.supachat.xanathar.XanatharLanguage;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class XanatharPSIFileRoot extends PsiFileBase implements ScopeNode {
    public XanatharPSIFileRoot(@NotNull FileViewProvider viewProvider) {
        super(viewProvider, XanatharLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public FileType getFileType() {
        return XanatharFileType.INSTANCE;
    }

    @Override
    public String toString() {
        return "Xanathar Language file";
    }

    @Override
    public Icon getIcon(int flags) {
        return XanatharIcons.FILE;
    }

    /** Return null since a file scope has no enclosing scope. It is
     *  not itself in a scope.
     */
    @Override
    public ScopeNode getContext() {
        return null;
    }

    @Nullable
    @Override
    public PsiElement resolve(PsiNamedElement element) {
//		System.out.println(getClass().getSimpleName()+
//		                   ".resolve("+element.getName()+
//		                   " at "+Integer.toHexString(element.hashCode())+")");
//        if ( element.getParent() instanceof CallSubtree ) {
//            return SymtabUtils.resolve(this, XanatharLanguage.INSTANCE,
//                    element, "/script/function/ID");
//        }
        return SymtabUtils.resolve(this, XanatharLanguage.INSTANCE,
                element, "/");
    }
}