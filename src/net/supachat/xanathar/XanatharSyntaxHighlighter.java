package net.supachat.xanathar;

import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase;
import com.intellij.psi.tree.IElementType;
import org.antlr.intellij.adaptor.lexer.ANTLRLexerAdaptor;
import org.antlr.intellij.adaptor.lexer.PSIElementTypeFactory;
import org.antlr.intellij.adaptor.lexer.TokenIElementType;
import net.supachat.xanathar.grammar.XanatharLexer;
import net.supachat.xanathar.grammar.XanatharParser;
import org.jetbrains.annotations.NotNull;
import static com.intellij.openapi.editor.colors.TextAttributesKey.createTextAttributesKey;

/** A highlighter is really just a mapping from token type to
 *  some text attributes using {@link #getTokenHighlights(IElementType)}.
 *  The reason that it returns an array, TextAttributesKey[], is
 *  that you might want to mix the attributes of a few known highlighters.
 *  A {@link TextAttributesKey} is just a name for that a theme
 *  or IDE skin can set. For example, {@link com.intellij.openapi.editor.DefaultLanguageHighlighterColors#KEYWORD}
 *  is the key that maps to what identifiers look like in the editor.
 *  To change it, see dialog: Editor > Colors & Fonts > Language Defaults.
 *
 *  From <a href="http://www.jetbrains.org/intellij/sdk/docs/reference_guide/custom_language_support/syntax_highlighting_and_error_highlighting.html">doc</a>:
 *  "The mapping of the TextAttributesKey to specific attributes used
 *  in an editor is defined by the EditorColorsScheme class, and can
 *  be configured by the user if the plugin provides an appropriate
 *  configuration interface.
 *  ...
 *  The syntax highlighter returns the {@link TextAttributesKey}
 * instances for each token type which needs special highlighting.
 * For highlighting lexer errors, the standard TextAttributesKey
 * for bad characters HighlighterColors.BAD_CHARACTER can be used."
 */
public class XanatharSyntaxHighlighter extends SyntaxHighlighterBase {
    private static final TextAttributesKey[] EMPTY_KEYS = new TextAttributesKey[0];
    public static final TextAttributesKey ID =
            createTextAttributesKey("XANATHAR_ID", DefaultLanguageHighlighterColors.IDENTIFIER);
    public static final TextAttributesKey KEYWORD =
            createTextAttributesKey("XANATHAR_KEYWORD", DefaultLanguageHighlighterColors.KEYWORD);
    public static final TextAttributesKey STRING =
            createTextAttributesKey("XANATHAR_STRING", DefaultLanguageHighlighterColors.STRING);
    public static final TextAttributesKey NUM =
            createTextAttributesKey("XANATHAR_NUMBER", DefaultLanguageHighlighterColors.NUMBER);
    public static final TextAttributesKey OP =
            createTextAttributesKey("XANATHAR_OP", DefaultLanguageHighlighterColors.OPERATION_SIGN);
    public static final TextAttributesKey LINE_COMMENT =
            createTextAttributesKey("XANATHAR_LINE_COMMENT", DefaultLanguageHighlighterColors.LINE_COMMENT);
    public static final TextAttributesKey BLOCK_COMMENT =
            createTextAttributesKey("XANATHAR_BLOCK_COMMENT", DefaultLanguageHighlighterColors.BLOCK_COMMENT);

    static {
        PSIElementTypeFactory.defineLanguageIElementTypes(XanatharLanguage.INSTANCE,
                XanatharParser.tokenNames,
                XanatharParser.ruleNames);
    }

    @NotNull
    @Override
    public Lexer getHighlightingLexer() {
        XanatharLexer lexer = new XanatharLexer(null);
        return new ANTLRLexerAdaptor(XanatharLanguage.INSTANCE, lexer);
    }

    @NotNull
    @Override
    public TextAttributesKey[] getTokenHighlights(IElementType tokenType) {
        if ( !(tokenType instanceof TokenIElementType) ) return EMPTY_KEYS;
        TokenIElementType myType = (TokenIElementType)tokenType;
        int ttype = myType.getANTLRTokenType();
        TextAttributesKey attrKey;
        switch ( ttype ) {
            case XanatharLexer.WORD :
                attrKey = ID;
                break;
            case XanatharLexer.TEMPLATE:
            case XanatharLexer.MUT:
            case XanatharLexer.LET:
            case XanatharLexer.DEFAULT:
            case XanatharLexer.EXTENDS:
            case XanatharLexer.WHILE:
            case XanatharLexer.IF:
            case XanatharLexer.HOOK:
            case XanatharLexer.IS:
            case XanatharLexer.OVERRIDE:
            case XanatharLexer.SEALED:
            case XanatharLexer.LOCK:
            case XanatharLexer.SKIP:
            case XanatharLexer.INTERFACE:
            case XanatharLexer.SIZEOF:
            case XanatharLexer.STATIC:
            case XanatharLexer.UNLOCK:
            case XanatharLexer.FOR:
            case XanatharLexer.NEW:
            case XanatharLexer.USE:
            case XanatharLexer.RET:
            case XanatharLexer.MATCH:
            case XanatharLexer.CLASS:
            case XanatharLexer.ELSE:
            case XanatharLexer.VIRTUAL:
            case XanatharLexer.BREAK:
            case XanatharLexer.TYPEOF:
            case XanatharLexer.DECLARE:
            case XanatharLexer.FN:
            case XanatharLexer.LOCKED:
            case XanatharLexer.AS:
                attrKey = KEYWORD;
                break;
            case XanatharLexer.STR :
                attrKey = STRING;
                break;
            case XanatharLexer.COMMENT :
                attrKey = LINE_COMMENT;
                break;
            case XanatharLexer.NUMBER:
            case XanatharLexer.FLOAT:
                attrKey = NUM;
                break;
            case XanatharLexer.ARROW:
            case XanatharLexer.PLUS:
            case XanatharLexer.MINUS:
            case XanatharLexer.TIMES:
            case XanatharLexer.DIV:
            case XanatharLexer.MOD:
            case XanatharLexer.ROL:
            case XanatharLexer.ROR:
            case XanatharLexer.LAMBDA:
            case XanatharLexer.LEFT_ANGLE:
            case XanatharLexer.RIGHT_ANGLE:
            case XanatharLexer.GEQ:
            case XanatharLexer.LEQ:
            case XanatharLexer.DOT:
            case XanatharLexer.AMPERSAND:
            case XanatharLexer.PIPE:
            case XanatharLexer.CARET:
                attrKey = OP;
                break;
            default :
                return EMPTY_KEYS;
        }
        return new TextAttributesKey[] {attrKey};
    }
}