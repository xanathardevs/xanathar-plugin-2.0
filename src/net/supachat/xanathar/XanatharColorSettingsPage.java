package net.supachat.xanathar;

import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.openapi.options.colors.*;
import org.jetbrains.annotations.*;

import javax.swing.*;
import java.util.Map;

public class XanatharColorSettingsPage implements ColorSettingsPage {
    private static final AttributesDescriptor[] DESCRIPTORS = new AttributesDescriptor[]{
            new AttributesDescriptor("ID", XanatharSyntaxHighlighter.ID),
            new AttributesDescriptor("Keyword", XanatharSyntaxHighlighter.KEYWORD),
            new AttributesDescriptor("Line Comment", XanatharSyntaxHighlighter.LINE_COMMENT),
            new AttributesDescriptor("String", XanatharSyntaxHighlighter.STRING),
            new AttributesDescriptor("Number", XanatharSyntaxHighlighter.NUM),
            new AttributesDescriptor("Operator", XanatharSyntaxHighlighter.OP),
    };

    @Nullable
    @Override
    public Icon getIcon() {
        return XanatharIcons.FILE;
    }

    @NotNull
    @Override
    public SyntaxHighlighter getHighlighter() {
        return new XanatharSyntaxHighlighter();
    }

    @NotNull
    @Override
    public String getDemoText() {
        return "use stdio;\n" +
                "\n" +
                "printf[\"Hello, world!\"];\n" +
                "\n" +
                "# Hello, programmer!\n" +
                "\n" +
                "if[$x > 0] {\n" +
                "\tprintf[\"$x is positive\"];\n" +
                "}";
    }

    @Nullable
    @Override
    public Map<String, TextAttributesKey> getAdditionalHighlightingTagToDescriptorMap() {
        return null;
    }

    @NotNull
    @Override
    public AttributesDescriptor[] getAttributeDescriptors() {
        return DESCRIPTORS;
    }

    @NotNull
    @Override
    public ColorDescriptor[] getColorDescriptors() {
        return ColorDescriptor.EMPTY_ARRAY;
    }

    @NotNull
    @Override
    public String getDisplayName() {
        return "Xanathar";
    }
}

