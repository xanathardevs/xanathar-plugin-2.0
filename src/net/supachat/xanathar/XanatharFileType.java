package net.supachat.xanathar;

import com.intellij.openapi.fileTypes.LanguageFileType;
import org.jetbrains.annotations.*;

import javax.swing.*;

public class XanatharFileType extends LanguageFileType {
    public static final XanatharFileType INSTANCE = new XanatharFileType();

    private XanatharFileType() {
        super(XanatharLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public String getName() {
        return "Xanathar file";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Xanathar language file";
    }

    @NotNull
    @Override
    public String getDefaultExtension() {
        return "xan";
    }

    @Nullable
    @Override
    public Icon getIcon() {
        return XanatharIcons.FILE;
    }
}