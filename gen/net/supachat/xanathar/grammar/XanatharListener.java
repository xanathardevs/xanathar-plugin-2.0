// Generated from /home/proc-daemon/Dropbox/XanatharPluginAntlr/src/net/supachat/xanathar/grammar/Xanathar.g4 by ANTLR 4.7.2
package net.supachat.xanathar.grammar;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link XanatharParser}.
 */
public interface XanatharListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link XanatharParser#prep}.
	 * @param ctx the parse tree
	 */
	void enterPrep(XanatharParser.PrepContext ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#prep}.
	 * @param ctx the parse tree
	 */
	void exitPrep(XanatharParser.PrepContext ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#cast}.
	 * @param ctx the parse tree
	 */
	void enterCast(XanatharParser.CastContext ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#cast}.
	 * @param ctx the parse tree
	 */
	void exitCast(XanatharParser.CastContext ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#declare_statement}.
	 * @param ctx the parse tree
	 */
	void enterDeclare_statement(XanatharParser.Declare_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#declare_statement}.
	 * @param ctx the parse tree
	 */
	void exitDeclare_statement(XanatharParser.Declare_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#variable_setting}.
	 * @param ctx the parse tree
	 */
	void enterVariable_setting(XanatharParser.Variable_settingContext ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#variable_setting}.
	 * @param ctx the parse tree
	 */
	void exitVariable_setting(XanatharParser.Variable_settingContext ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#let}.
	 * @param ctx the parse tree
	 */
	void enterLet(XanatharParser.LetContext ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#let}.
	 * @param ctx the parse tree
	 */
	void exitLet(XanatharParser.LetContext ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#class_}.
	 * @param ctx the parse tree
	 */
	void enterClass_(XanatharParser.Class_Context ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#class_}.
	 * @param ctx the parse tree
	 */
	void exitClass_(XanatharParser.Class_Context ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#class__specs}.
	 * @param ctx the parse tree
	 */
	void enterClass__specs(XanatharParser.Class__specsContext ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#class__specs}.
	 * @param ctx the parse tree
	 */
	void exitClass__specs(XanatharParser.Class__specsContext ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#extends_}.
	 * @param ctx the parse tree
	 */
	void enterExtends_(XanatharParser.Extends_Context ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#extends_}.
	 * @param ctx the parse tree
	 */
	void exitExtends_(XanatharParser.Extends_Context ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#types}.
	 * @param ctx the parse tree
	 */
	void enterTypes(XanatharParser.TypesContext ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#types}.
	 * @param ctx the parse tree
	 */
	void exitTypes(XanatharParser.TypesContext ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#inheritance}.
	 * @param ctx the parse tree
	 */
	void enterInheritance(XanatharParser.InheritanceContext ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#inheritance}.
	 * @param ctx the parse tree
	 */
	void exitInheritance(XanatharParser.InheritanceContext ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#fncall}.
	 * @param ctx the parse tree
	 */
	void enterFncall(XanatharParser.FncallContext ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#fncall}.
	 * @param ctx the parse tree
	 */
	void exitFncall(XanatharParser.FncallContext ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#fn}.
	 * @param ctx the parse tree
	 */
	void enterFn(XanatharParser.FnContext ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#fn}.
	 * @param ctx the parse tree
	 */
	void exitFn(XanatharParser.FnContext ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#hook}.
	 * @param ctx the parse tree
	 */
	void enterHook(XanatharParser.HookContext ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#hook}.
	 * @param ctx the parse tree
	 */
	void exitHook(XanatharParser.HookContext ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#template_}.
	 * @param ctx the parse tree
	 */
	void enterTemplate_(XanatharParser.Template_Context ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#template_}.
	 * @param ctx the parse tree
	 */
	void exitTemplate_(XanatharParser.Template_Context ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#override}.
	 * @param ctx the parse tree
	 */
	void enterOverride(XanatharParser.OverrideContext ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#override}.
	 * @param ctx the parse tree
	 */
	void exitOverride(XanatharParser.OverrideContext ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#override_param}.
	 * @param ctx the parse tree
	 */
	void enterOverride_param(XanatharParser.Override_paramContext ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#override_param}.
	 * @param ctx the parse tree
	 */
	void exitOverride_param(XanatharParser.Override_paramContext ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#lambda}.
	 * @param ctx the parse tree
	 */
	void enterLambda(XanatharParser.LambdaContext ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#lambda}.
	 * @param ctx the parse tree
	 */
	void exitLambda(XanatharParser.LambdaContext ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#if_}.
	 * @param ctx the parse tree
	 */
	void enterIf_(XanatharParser.If_Context ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#if_}.
	 * @param ctx the parse tree
	 */
	void exitIf_(XanatharParser.If_Context ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#else_}.
	 * @param ctx the parse tree
	 */
	void enterElse_(XanatharParser.Else_Context ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#else_}.
	 * @param ctx the parse tree
	 */
	void exitElse_(XanatharParser.Else_Context ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#for_}.
	 * @param ctx the parse tree
	 */
	void enterFor_(XanatharParser.For_Context ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#for_}.
	 * @param ctx the parse tree
	 */
	void exitFor_(XanatharParser.For_Context ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#while_}.
	 * @param ctx the parse tree
	 */
	void enterWhile_(XanatharParser.While_Context ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#while_}.
	 * @param ctx the parse tree
	 */
	void exitWhile_(XanatharParser.While_Context ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#ret}.
	 * @param ctx the parse tree
	 */
	void enterRet(XanatharParser.RetContext ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#ret}.
	 * @param ctx the parse tree
	 */
	void exitRet(XanatharParser.RetContext ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#match_}.
	 * @param ctx the parse tree
	 */
	void enterMatch_(XanatharParser.Match_Context ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#match_}.
	 * @param ctx the parse tree
	 */
	void exitMatch_(XanatharParser.Match_Context ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#match_arm}.
	 * @param ctx the parse tree
	 */
	void enterMatch_arm(XanatharParser.Match_armContext ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#match_arm}.
	 * @param ctx the parse tree
	 */
	void exitMatch_arm(XanatharParser.Match_armContext ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#default_arm}.
	 * @param ctx the parse tree
	 */
	void enterDefault_arm(XanatharParser.Default_armContext ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#default_arm}.
	 * @param ctx the parse tree
	 */
	void exitDefault_arm(XanatharParser.Default_armContext ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#ptr}.
	 * @param ctx the parse tree
	 */
	void enterPtr(XanatharParser.PtrContext ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#ptr}.
	 * @param ctx the parse tree
	 */
	void exitPtr(XanatharParser.PtrContext ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#deref}.
	 * @param ctx the parse tree
	 */
	void enterDeref(XanatharParser.DerefContext ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#deref}.
	 * @param ctx the parse tree
	 */
	void exitDeref(XanatharParser.DerefContext ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#new_}.
	 * @param ctx the parse tree
	 */
	void enterNew_(XanatharParser.New_Context ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#new_}.
	 * @param ctx the parse tree
	 */
	void exitNew_(XanatharParser.New_Context ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#lock}.
	 * @param ctx the parse tree
	 */
	void enterLock(XanatharParser.LockContext ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#lock}.
	 * @param ctx the parse tree
	 */
	void exitLock(XanatharParser.LockContext ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#unlock}.
	 * @param ctx the parse tree
	 */
	void enterUnlock(XanatharParser.UnlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#unlock}.
	 * @param ctx the parse tree
	 */
	void exitUnlock(XanatharParser.UnlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#inc}.
	 * @param ctx the parse tree
	 */
	void enterInc(XanatharParser.IncContext ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#inc}.
	 * @param ctx the parse tree
	 */
	void exitInc(XanatharParser.IncContext ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#dec}.
	 * @param ctx the parse tree
	 */
	void enterDec(XanatharParser.DecContext ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#dec}.
	 * @param ctx the parse tree
	 */
	void exitDec(XanatharParser.DecContext ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#reg}.
	 * @param ctx the parse tree
	 */
	void enterReg(XanatharParser.RegContext ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#reg}.
	 * @param ctx the parse tree
	 */
	void exitReg(XanatharParser.RegContext ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#reg_set}.
	 * @param ctx the parse tree
	 */
	void enterReg_set(XanatharParser.Reg_setContext ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#reg_set}.
	 * @param ctx the parse tree
	 */
	void exitReg_set(XanatharParser.Reg_setContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BinaryNot}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterBinaryNot(XanatharParser.BinaryNotContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BinaryNot}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitBinaryNot(XanatharParser.BinaryNotContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LambdaCreation}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterLambdaCreation(XanatharParser.LambdaCreationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LambdaCreation}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitLambdaCreation(XanatharParser.LambdaCreationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LessOrEqual}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterLessOrEqual(XanatharParser.LessOrEqualContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LessOrEqual}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitLessOrEqual(XanatharParser.LessOrEqualContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Multiplication}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterMultiplication(XanatharParser.MultiplicationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Multiplication}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitMultiplication(XanatharParser.MultiplicationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BinaryAnd}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterBinaryAnd(XanatharParser.BinaryAndContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BinaryAnd}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitBinaryAnd(XanatharParser.BinaryAndContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Bareword}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterBareword(XanatharParser.BarewordContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Bareword}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitBareword(XanatharParser.BarewordContext ctx);
	/**
	 * Enter a parse tree produced by the {@code CallSealedClass}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterCallSealedClass(XanatharParser.CallSealedClassContext ctx);
	/**
	 * Exit a parse tree produced by the {@code CallSealedClass}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitCallSealedClass(XanatharParser.CallSealedClassContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BinaryXor}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterBinaryXor(XanatharParser.BinaryXorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BinaryXor}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitBinaryXor(XanatharParser.BinaryXorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code GetSizeOf}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterGetSizeOf(XanatharParser.GetSizeOfContext ctx);
	/**
	 * Exit a parse tree produced by the {@code GetSizeOf}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitGetSizeOf(XanatharParser.GetSizeOfContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Pointer}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterPointer(XanatharParser.PointerContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Pointer}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitPointer(XanatharParser.PointerContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Is}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterIs(XanatharParser.IsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Is}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitIs(XanatharParser.IsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code String}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterString(XanatharParser.StringContext ctx);
	/**
	 * Exit a parse tree produced by the {@code String}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitString(XanatharParser.StringContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ArrayIndex}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterArrayIndex(XanatharParser.ArrayIndexContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ArrayIndex}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitArrayIndex(XanatharParser.ArrayIndexContext ctx);
	/**
	 * Enter a parse tree produced by the {@code NewCall}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNewCall(XanatharParser.NewCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code NewCall}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNewCall(XanatharParser.NewCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Equal}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterEqual(XanatharParser.EqualContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Equal}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitEqual(XanatharParser.EqualContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Increment}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterIncrement(XanatharParser.IncrementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Increment}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitIncrement(XanatharParser.IncrementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code GetRegister}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterGetRegister(XanatharParser.GetRegisterContext ctx);
	/**
	 * Exit a parse tree produced by the {@code GetRegister}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitGetRegister(XanatharParser.GetRegisterContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BinaryOr}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterBinaryOr(XanatharParser.BinaryOrContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BinaryOr}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitBinaryOr(XanatharParser.BinaryOrContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Division}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterDivision(XanatharParser.DivisionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Division}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitDivision(XanatharParser.DivisionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code FunctionCall}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterFunctionCall(XanatharParser.FunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code FunctionCall}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitFunctionCall(XanatharParser.FunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Less}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterLess(XanatharParser.LessContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Less}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitLess(XanatharParser.LessContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Parentheses}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterParentheses(XanatharParser.ParenthesesContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Parentheses}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitParentheses(XanatharParser.ParenthesesContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Dereference}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterDereference(XanatharParser.DereferenceContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Dereference}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitDereference(XanatharParser.DereferenceContext ctx);
	/**
	 * Enter a parse tree produced by the {@code GenericCall}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterGenericCall(XanatharParser.GenericCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code GenericCall}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitGenericCall(XanatharParser.GenericCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code GetTypeOf}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterGetTypeOf(XanatharParser.GetTypeOfContext ctx);
	/**
	 * Exit a parse tree produced by the {@code GetTypeOf}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitGetTypeOf(XanatharParser.GetTypeOfContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Addition}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAddition(XanatharParser.AdditionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Addition}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAddition(XanatharParser.AdditionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code NotEqual}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNotEqual(XanatharParser.NotEqualContext ctx);
	/**
	 * Exit a parse tree produced by the {@code NotEqual}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNotEqual(XanatharParser.NotEqualContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Decrement}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterDecrement(XanatharParser.DecrementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Decrement}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitDecrement(XanatharParser.DecrementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Reference}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterReference(XanatharParser.ReferenceContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Reference}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitReference(XanatharParser.ReferenceContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Modulo}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterModulo(XanatharParser.ModuloContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Modulo}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitModulo(XanatharParser.ModuloContext ctx);
	/**
	 * Enter a parse tree produced by the {@code RawVariable}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterRawVariable(XanatharParser.RawVariableContext ctx);
	/**
	 * Exit a parse tree produced by the {@code RawVariable}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitRawVariable(XanatharParser.RawVariableContext ctx);
	/**
	 * Enter a parse tree produced by the {@code VariablePhrase}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterVariablePhrase(XanatharParser.VariablePhraseContext ctx);
	/**
	 * Exit a parse tree produced by the {@code VariablePhrase}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitVariablePhrase(XanatharParser.VariablePhraseContext ctx);
	/**
	 * Enter a parse tree produced by the {@code GreaterOrEqual}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterGreaterOrEqual(XanatharParser.GreaterOrEqualContext ctx);
	/**
	 * Exit a parse tree produced by the {@code GreaterOrEqual}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitGreaterOrEqual(XanatharParser.GreaterOrEqualContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Range}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterRange(XanatharParser.RangeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Range}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitRange(XanatharParser.RangeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code NumberConst}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNumberConst(XanatharParser.NumberConstContext ctx);
	/**
	 * Exit a parse tree produced by the {@code NumberConst}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNumberConst(XanatharParser.NumberConstContext ctx);
	/**
	 * Enter a parse tree produced by the {@code FloatConst}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterFloatConst(XanatharParser.FloatConstContext ctx);
	/**
	 * Exit a parse tree produced by the {@code FloatConst}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitFloatConst(XanatharParser.FloatConstContext ctx);
	/**
	 * Enter a parse tree produced by the {@code RotateRight}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterRotateRight(XanatharParser.RotateRightContext ctx);
	/**
	 * Exit a parse tree produced by the {@code RotateRight}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitRotateRight(XanatharParser.RotateRightContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Casting}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterCasting(XanatharParser.CastingContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Casting}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitCasting(XanatharParser.CastingContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Subtraction}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterSubtraction(XanatharParser.SubtractionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Subtraction}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitSubtraction(XanatharParser.SubtractionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Greater}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterGreater(XanatharParser.GreaterContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Greater}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitGreater(XanatharParser.GreaterContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Spec}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterSpec(XanatharParser.SpecContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Spec}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitSpec(XanatharParser.SpecContext ctx);
	/**
	 * Enter a parse tree produced by the {@code RotateLeft}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterRotateLeft(XanatharParser.RotateLeftContext ctx);
	/**
	 * Exit a parse tree produced by the {@code RotateLeft}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitRotateLeft(XanatharParser.RotateLeftContext ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#phrase}.
	 * @param ctx the parse tree
	 */
	void enterPhrase(XanatharParser.PhraseContext ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#phrase}.
	 * @param ctx the parse tree
	 */
	void exitPhrase(XanatharParser.PhraseContext ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#start}.
	 * @param ctx the parse tree
	 */
	void enterStart(XanatharParser.StartContext ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#start}.
	 * @param ctx the parse tree
	 */
	void exitStart(XanatharParser.StartContext ctx);
	/**
	 * Enter a parse tree produced by {@link XanatharParser#asm_}.
	 * @param ctx the parse tree
	 */
	void enterAsm_(XanatharParser.Asm_Context ctx);
	/**
	 * Exit a parse tree produced by {@link XanatharParser#asm_}.
	 * @param ctx the parse tree
	 */
	void exitAsm_(XanatharParser.Asm_Context ctx);
}