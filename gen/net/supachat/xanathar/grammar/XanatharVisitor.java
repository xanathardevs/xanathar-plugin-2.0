// Generated from /home/proc-daemon/Dropbox/XanatharPluginAntlr/src/net/supachat/xanathar/grammar/Xanathar.g4 by ANTLR 4.7.2
package net.supachat.xanathar.grammar;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link XanatharParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface XanatharVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link XanatharParser#prep}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrep(XanatharParser.PrepContext ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#cast}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCast(XanatharParser.CastContext ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#declare_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclare_statement(XanatharParser.Declare_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#variable_setting}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable_setting(XanatharParser.Variable_settingContext ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#let}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLet(XanatharParser.LetContext ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#class_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClass_(XanatharParser.Class_Context ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#class__specs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClass__specs(XanatharParser.Class__specsContext ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#extends_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExtends_(XanatharParser.Extends_Context ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#types}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypes(XanatharParser.TypesContext ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#inheritance}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInheritance(XanatharParser.InheritanceContext ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#fncall}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFncall(XanatharParser.FncallContext ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#fn}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFn(XanatharParser.FnContext ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#hook}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHook(XanatharParser.HookContext ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#template_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTemplate_(XanatharParser.Template_Context ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#override}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOverride(XanatharParser.OverrideContext ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#override_param}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOverride_param(XanatharParser.Override_paramContext ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#lambda}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLambda(XanatharParser.LambdaContext ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#if_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_(XanatharParser.If_Context ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#else_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElse_(XanatharParser.Else_Context ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#for_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFor_(XanatharParser.For_Context ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#while_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhile_(XanatharParser.While_Context ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#ret}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRet(XanatharParser.RetContext ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#match_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMatch_(XanatharParser.Match_Context ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#match_arm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMatch_arm(XanatharParser.Match_armContext ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#default_arm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefault_arm(XanatharParser.Default_armContext ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#ptr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPtr(XanatharParser.PtrContext ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#deref}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeref(XanatharParser.DerefContext ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#new_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNew_(XanatharParser.New_Context ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#lock}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLock(XanatharParser.LockContext ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#unlock}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnlock(XanatharParser.UnlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#inc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInc(XanatharParser.IncContext ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#dec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDec(XanatharParser.DecContext ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#reg}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReg(XanatharParser.RegContext ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#reg_set}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReg_set(XanatharParser.Reg_setContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BinaryNot}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinaryNot(XanatharParser.BinaryNotContext ctx);
	/**
	 * Visit a parse tree produced by the {@code LambdaCreation}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLambdaCreation(XanatharParser.LambdaCreationContext ctx);
	/**
	 * Visit a parse tree produced by the {@code LessOrEqual}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLessOrEqual(XanatharParser.LessOrEqualContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Multiplication}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiplication(XanatharParser.MultiplicationContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BinaryAnd}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinaryAnd(XanatharParser.BinaryAndContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Bareword}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBareword(XanatharParser.BarewordContext ctx);
	/**
	 * Visit a parse tree produced by the {@code CallSealedClass}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCallSealedClass(XanatharParser.CallSealedClassContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BinaryXor}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinaryXor(XanatharParser.BinaryXorContext ctx);
	/**
	 * Visit a parse tree produced by the {@code GetSizeOf}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGetSizeOf(XanatharParser.GetSizeOfContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Pointer}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPointer(XanatharParser.PointerContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Is}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIs(XanatharParser.IsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code String}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitString(XanatharParser.StringContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ArrayIndex}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrayIndex(XanatharParser.ArrayIndexContext ctx);
	/**
	 * Visit a parse tree produced by the {@code NewCall}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNewCall(XanatharParser.NewCallContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Equal}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEqual(XanatharParser.EqualContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Increment}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIncrement(XanatharParser.IncrementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code GetRegister}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGetRegister(XanatharParser.GetRegisterContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BinaryOr}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinaryOr(XanatharParser.BinaryOrContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Division}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDivision(XanatharParser.DivisionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code FunctionCall}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionCall(XanatharParser.FunctionCallContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Less}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLess(XanatharParser.LessContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Parentheses}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParentheses(XanatharParser.ParenthesesContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Dereference}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDereference(XanatharParser.DereferenceContext ctx);
	/**
	 * Visit a parse tree produced by the {@code GenericCall}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGenericCall(XanatharParser.GenericCallContext ctx);
	/**
	 * Visit a parse tree produced by the {@code GetTypeOf}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGetTypeOf(XanatharParser.GetTypeOfContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Addition}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAddition(XanatharParser.AdditionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code NotEqual}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNotEqual(XanatharParser.NotEqualContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Decrement}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDecrement(XanatharParser.DecrementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Reference}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReference(XanatharParser.ReferenceContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Modulo}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModulo(XanatharParser.ModuloContext ctx);
	/**
	 * Visit a parse tree produced by the {@code RawVariable}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRawVariable(XanatharParser.RawVariableContext ctx);
	/**
	 * Visit a parse tree produced by the {@code VariablePhrase}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariablePhrase(XanatharParser.VariablePhraseContext ctx);
	/**
	 * Visit a parse tree produced by the {@code GreaterOrEqual}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGreaterOrEqual(XanatharParser.GreaterOrEqualContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Range}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRange(XanatharParser.RangeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code NumberConst}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumberConst(XanatharParser.NumberConstContext ctx);
	/**
	 * Visit a parse tree produced by the {@code FloatConst}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFloatConst(XanatharParser.FloatConstContext ctx);
	/**
	 * Visit a parse tree produced by the {@code RotateRight}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRotateRight(XanatharParser.RotateRightContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Casting}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCasting(XanatharParser.CastingContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Subtraction}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubtraction(XanatharParser.SubtractionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Greater}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGreater(XanatharParser.GreaterContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Spec}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSpec(XanatharParser.SpecContext ctx);
	/**
	 * Visit a parse tree produced by the {@code RotateLeft}
	 * labeled alternative in {@link XanatharParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRotateLeft(XanatharParser.RotateLeftContext ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#phrase}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPhrase(XanatharParser.PhraseContext ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#start}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStart(XanatharParser.StartContext ctx);
	/**
	 * Visit a parse tree produced by {@link XanatharParser#asm_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAsm_(XanatharParser.Asm_Context ctx);
}