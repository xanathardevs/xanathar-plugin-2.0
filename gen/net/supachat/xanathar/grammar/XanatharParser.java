// Generated from /home/proc-daemon/Dropbox/XanatharPluginAntlr/src/net/supachat/xanathar/grammar/Xanathar.g4 by ANTLR 4.7.2
package net.supachat.xanathar.grammar;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class XanatharParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, COMMENT=26, DIGIT=27, USE=28, DECLARE=29, LOCKED=30, MUT=31, 
		LET=32, STATIC=33, SEALED=34, EXTENDS=35, INTERFACE=36, FN=37, HOOK=38, 
		TEMPLATE=39, CLASS=40, VIRTUAL=41, AS=42, OVERRIDE=43, IF=44, ELSE=45, 
		IS=46, FOR=47, WHILE=48, RET=49, MATCH=50, DEFAULT=51, BREAK=52, SKIP_=53, 
		LOCK=54, UNLOCK=55, SIZEOF=56, TYPEOF=57, NEW=58, ARROW=59, PLUS=60, MINUS=61, 
		TIMES=62, DIV=63, MOD=64, ROL=65, ROR=66, LAMBDA=67, LEFT_ANGLE=68, RIGHT_ANGLE=69, 
		GEQ=70, LEQ=71, DOT=72, AMPERSAND=73, PIPE=74, CARET=75, PARAMS=76, VARIABLE_PHRASE=77, 
		AS_PHRASE=78, WORD=79, USE_STATEMENT=80, RAW_VAR=81, TYPESPEC=82, INTERFACE_=83, 
		INTERFACE_FN=84, DECLARED_VAR=85, GP=86, OP=87, CLASSES=88, CLASS_=89, 
		VIRTUAL_FN=90, OVERRIDE_NAME=91, REF=92, NUMBER=93, FLOAT=94, STR=95, 
		WS=96, ERRCHAR=97;
	public static final int
		RULE_prep = 0, RULE_cast = 1, RULE_declare_statement = 2, RULE_variable_setting = 3, 
		RULE_let = 4, RULE_class_ = 5, RULE_class__specs = 6, RULE_extends_ = 7, 
		RULE_types = 8, RULE_inheritance = 9, RULE_fncall = 10, RULE_fn = 11, 
		RULE_hook = 12, RULE_template_ = 13, RULE_override = 14, RULE_override_param = 15, 
		RULE_lambda = 16, RULE_if_ = 17, RULE_else_ = 18, RULE_for_ = 19, RULE_while_ = 20, 
		RULE_ret = 21, RULE_match_ = 22, RULE_match_arm = 23, RULE_default_arm = 24, 
		RULE_ptr = 25, RULE_deref = 26, RULE_new_ = 27, RULE_lock = 28, RULE_unlock = 29, 
		RULE_inc = 30, RULE_dec = 31, RULE_reg = 32, RULE_reg_set = 33, RULE_expression = 34, 
		RULE_phrase = 35, RULE_start = 36, RULE_asm_ = 37;
	private static String[] makeRuleNames() {
		return new String[] {
			"prep", "cast", "declare_statement", "variable_setting", "let", "class_", 
			"class__specs", "extends_", "types", "inheritance", "fncall", "fn", "hook", 
			"template_", "override", "override_param", "lambda", "if_", "else_", 
			"for_", "while_", "ret", "match_", "match_arm", "default_arm", "ptr", 
			"deref", "new_", "lock", "unlock", "inc", "dec", "reg", "reg_set", "expression", 
			"phrase", "start", "asm_"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'!=!'", "'['", "']'", "'('", "')'", "'='", "'class_'", "':'", 
			"'{'", "'}'", "','", "'@'", "';'", "'=>'", "'new_'", "'++'", "'--'", 
			"'<-'", "'::'", "'..'", "'!='", "'=='", "'!'", "'__asm__'", "'__end-asm__'", 
			null, null, "'use'", "'declare'", "'locked'", "'mut'", "'let'", "'static'", 
			"'sealed'", "'extends'", "'interface'", "'fn'", "'hook'", "'template'", 
			"'class'", "'virtual'", "'as'", "'override'", "'if'", "'else'", "'is'", 
			"'for'", "'while'", "'ret'", "'match'", "'default'", "'break'", "'SKIP_'", 
			"'lock'", "'unlock'", "'sizeof'", "'typeof'", "'new'", "'->'", "'+'", 
			"'-'", "'*'", "'/'", "'%'", "'<<'", "'>>'", "'\u03BB'", "'<'", "'>'", 
			"'<='", "'>='", "'.'", "'&'", "'|'", "'^'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, "COMMENT", "DIGIT", "USE", "DECLARE", "LOCKED", "MUT", "LET", 
			"STATIC", "SEALED", "EXTENDS", "INTERFACE", "FN", "HOOK", "TEMPLATE", 
			"CLASS", "VIRTUAL", "AS", "OVERRIDE", "IF", "ELSE", "IS", "FOR", "WHILE", 
			"RET", "MATCH", "DEFAULT", "BREAK", "SKIP_", "LOCK", "UNLOCK", "SIZEOF", 
			"TYPEOF", "NEW", "ARROW", "PLUS", "MINUS", "TIMES", "DIV", "MOD", "ROL", 
			"ROR", "LAMBDA", "LEFT_ANGLE", "RIGHT_ANGLE", "GEQ", "LEQ", "DOT", "AMPERSAND", 
			"PIPE", "CARET", "PARAMS", "VARIABLE_PHRASE", "AS_PHRASE", "WORD", "USE_STATEMENT", 
			"RAW_VAR", "TYPESPEC", "INTERFACE_", "INTERFACE_FN", "DECLARED_VAR", 
			"GP", "OP", "CLASSES", "CLASS_", "VIRTUAL_FN", "OVERRIDE_NAME", "REF", 
			"NUMBER", "FLOAT", "STR", "WS", "ERRCHAR"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Xanathar.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public XanatharParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class PrepContext extends ParserRuleContext {
		public PrepContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prep; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterPrep(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitPrep(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitPrep(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrepContext prep() throws RecognitionException {
		PrepContext _localctx = new PrepContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_prep);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(76);
			match(T__0);
			setState(78); 
			_errHandler.sync(this);
			_alt = 1+1;
			do {
				switch (_alt) {
				case 1+1:
					{
					{
					setState(77);
					matchWildcard();
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(80); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			} while ( _alt!=1 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
			setState(82);
			match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CastContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode AS_PHRASE() { return getToken(XanatharParser.AS_PHRASE, 0); }
		public CastContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cast; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterCast(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitCast(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitCast(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CastContext cast() throws RecognitionException {
		CastContext _localctx = new CastContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_cast);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(84);
			match(T__1);
			setState(85);
			expression(0);
			setState(86);
			match(AS_PHRASE);
			setState(87);
			match(T__2);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Declare_statementContext extends ParserRuleContext {
		public TerminalNode DECLARE() { return getToken(XanatharParser.DECLARE, 0); }
		public TerminalNode VARIABLE_PHRASE() { return getToken(XanatharParser.VARIABLE_PHRASE, 0); }
		public TerminalNode AS_PHRASE() { return getToken(XanatharParser.AS_PHRASE, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Declare_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declare_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterDeclare_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitDeclare_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitDeclare_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Declare_statementContext declare_statement() throws RecognitionException {
		Declare_statementContext _localctx = new Declare_statementContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_declare_statement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(89);
			match(DECLARE);
			setState(90);
			match(VARIABLE_PHRASE);
			setState(91);
			match(AS_PHRASE);
			setState(96);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__3) {
				{
				setState(92);
				match(T__3);
				setState(93);
				expression(0);
				setState(94);
				match(T__4);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Variable_settingContext extends ParserRuleContext {
		public TerminalNode VARIABLE_PHRASE() { return getToken(XanatharParser.VARIABLE_PHRASE, 0); }
		public TerminalNode REF() { return getToken(XanatharParser.REF, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public FncallContext fncall() {
			return getRuleContext(FncallContext.class,0);
		}
		public Variable_settingContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variable_setting; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterVariable_setting(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitVariable_setting(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitVariable_setting(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Variable_settingContext variable_setting() throws RecognitionException {
		Variable_settingContext _localctx = new Variable_settingContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_variable_setting);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(98);
			_la = _input.LA(1);
			if ( !(_la==VARIABLE_PHRASE || _la==REF) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(103);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__3) {
				{
				setState(99);
				match(T__3);
				setState(100);
				expression(0);
				setState(101);
				match(T__4);
				}
			}

			setState(105);
			match(T__5);
			setState(108);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
			case 1:
				{
				setState(106);
				expression(0);
				}
				break;
			case 2:
				{
				setState(107);
				fncall();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LetContext extends ParserRuleContext {
		public TerminalNode LET() { return getToken(XanatharParser.LET, 0); }
		public TerminalNode VARIABLE_PHRASE() { return getToken(XanatharParser.VARIABLE_PHRASE, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public LetContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_let; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterLet(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitLet(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitLet(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LetContext let() throws RecognitionException {
		LetContext _localctx = new LetContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_let);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(110);
			match(LET);
			setState(111);
			match(VARIABLE_PHRASE);
			setState(112);
			match(T__5);
			setState(113);
			expression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Class_Context extends ParserRuleContext {
		public Class__specsContext class__specs() {
			return getRuleContext(Class__specsContext.class,0);
		}
		public TerminalNode WORD() { return getToken(XanatharParser.WORD, 0); }
		public TerminalNode LEFT_ANGLE() { return getToken(XanatharParser.LEFT_ANGLE, 0); }
		public TypesContext types() {
			return getRuleContext(TypesContext.class,0);
		}
		public TerminalNode RIGHT_ANGLE() { return getToken(XanatharParser.RIGHT_ANGLE, 0); }
		public List<InheritanceContext> inheritance() {
			return getRuleContexts(InheritanceContext.class);
		}
		public InheritanceContext inheritance(int i) {
			return getRuleContext(InheritanceContext.class,i);
		}
		public List<PhraseContext> phrase() {
			return getRuleContexts(PhraseContext.class);
		}
		public PhraseContext phrase(int i) {
			return getRuleContext(PhraseContext.class,i);
		}
		public Class_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_class_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterClass_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitClass_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitClass_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Class_Context class_() throws RecognitionException {
		Class_Context _localctx = new Class_Context(_ctx, getState());
		enterRule(_localctx, 10, RULE_class_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(115);
			class__specs();
			setState(116);
			match(T__6);
			setState(117);
			match(WORD);
			setState(118);
			match(LEFT_ANGLE);
			setState(119);
			types();
			setState(120);
			match(RIGHT_ANGLE);
			setState(121);
			inheritance();
			setState(124);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__7) {
				{
				setState(122);
				match(T__7);
				setState(123);
				inheritance();
				}
			}

			setState(126);
			match(T__8);
			{
			setState(130);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__1) | (1L << T__3) | (1L << T__6) | (1L << T__14) | (1L << T__22) | (1L << T__23) | (1L << DECLARE) | (1L << LET) | (1L << STATIC) | (1L << SEALED) | (1L << EXTENDS) | (1L << FN) | (1L << HOOK) | (1L << TEMPLATE) | (1L << OVERRIDE) | (1L << IF) | (1L << FOR) | (1L << WHILE) | (1L << RET) | (1L << MATCH) | (1L << BREAK) | (1L << SKIP_) | (1L << LOCK) | (1L << UNLOCK) | (1L << SIZEOF) | (1L << TYPEOF) | (1L << NEW) | (1L << TIMES))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (MOD - 64)) | (1L << (LAMBDA - 64)) | (1L << (AMPERSAND - 64)) | (1L << (VARIABLE_PHRASE - 64)) | (1L << (WORD - 64)) | (1L << (USE_STATEMENT - 64)) | (1L << (RAW_VAR - 64)) | (1L << (INTERFACE_ - 64)) | (1L << (VIRTUAL_FN - 64)) | (1L << (REF - 64)) | (1L << (NUMBER - 64)) | (1L << (FLOAT - 64)) | (1L << (STR - 64)))) != 0)) {
				{
				{
				setState(127);
				phrase();
				}
				}
				setState(132);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
			setState(133);
			match(T__9);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Class__specsContext extends ParserRuleContext {
		public List<TerminalNode> STATIC() { return getTokens(XanatharParser.STATIC); }
		public TerminalNode STATIC(int i) {
			return getToken(XanatharParser.STATIC, i);
		}
		public List<TerminalNode> SEALED() { return getTokens(XanatharParser.SEALED); }
		public TerminalNode SEALED(int i) {
			return getToken(XanatharParser.SEALED, i);
		}
		public Class__specsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_class__specs; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterClass__specs(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitClass__specs(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitClass__specs(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Class__specsContext class__specs() throws RecognitionException {
		Class__specsContext _localctx = new Class__specsContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_class__specs);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(138);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==STATIC || _la==SEALED) {
				{
				{
				setState(135);
				_la = _input.LA(1);
				if ( !(_la==STATIC || _la==SEALED) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				}
				setState(140);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Extends_Context extends ParserRuleContext {
		public TerminalNode EXTENDS() { return getToken(XanatharParser.EXTENDS, 0); }
		public TerminalNode WORD() { return getToken(XanatharParser.WORD, 0); }
		public List<PhraseContext> phrase() {
			return getRuleContexts(PhraseContext.class);
		}
		public PhraseContext phrase(int i) {
			return getRuleContext(PhraseContext.class,i);
		}
		public Extends_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_extends_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterExtends_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitExtends_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitExtends_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Extends_Context extends_() throws RecognitionException {
		Extends_Context _localctx = new Extends_Context(_ctx, getState());
		enterRule(_localctx, 14, RULE_extends_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(141);
			match(EXTENDS);
			setState(142);
			match(WORD);
			setState(143);
			match(T__8);
			{
			setState(147);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__1) | (1L << T__3) | (1L << T__6) | (1L << T__14) | (1L << T__22) | (1L << T__23) | (1L << DECLARE) | (1L << LET) | (1L << STATIC) | (1L << SEALED) | (1L << EXTENDS) | (1L << FN) | (1L << HOOK) | (1L << TEMPLATE) | (1L << OVERRIDE) | (1L << IF) | (1L << FOR) | (1L << WHILE) | (1L << RET) | (1L << MATCH) | (1L << BREAK) | (1L << SKIP_) | (1L << LOCK) | (1L << UNLOCK) | (1L << SIZEOF) | (1L << TYPEOF) | (1L << NEW) | (1L << TIMES))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (MOD - 64)) | (1L << (LAMBDA - 64)) | (1L << (AMPERSAND - 64)) | (1L << (VARIABLE_PHRASE - 64)) | (1L << (WORD - 64)) | (1L << (USE_STATEMENT - 64)) | (1L << (RAW_VAR - 64)) | (1L << (INTERFACE_ - 64)) | (1L << (VIRTUAL_FN - 64)) | (1L << (REF - 64)) | (1L << (NUMBER - 64)) | (1L << (FLOAT - 64)) | (1L << (STR - 64)))) != 0)) {
				{
				{
				setState(144);
				phrase();
				}
				}
				setState(149);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
			setState(150);
			match(T__9);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypesContext extends ParserRuleContext {
		public List<TerminalNode> DECLARED_VAR() { return getTokens(XanatharParser.DECLARED_VAR); }
		public TerminalNode DECLARED_VAR(int i) {
			return getToken(XanatharParser.DECLARED_VAR, i);
		}
		public TypesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_types; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterTypes(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitTypes(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitTypes(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypesContext types() throws RecognitionException {
		TypesContext _localctx = new TypesContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_types);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(160);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==DECLARED_VAR) {
				{
				setState(156);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(152);
						match(DECLARED_VAR);
						setState(153);
						match(T__10);
						}
						} 
					}
					setState(158);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
				}
				setState(159);
				match(DECLARED_VAR);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InheritanceContext extends ParserRuleContext {
		public List<TerminalNode> WORD() { return getTokens(XanatharParser.WORD); }
		public TerminalNode WORD(int i) {
			return getToken(XanatharParser.WORD, i);
		}
		public InheritanceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_inheritance; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterInheritance(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitInheritance(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitInheritance(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InheritanceContext inheritance() throws RecognitionException {
		InheritanceContext _localctx = new InheritanceContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_inheritance);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(172);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__3) {
				{
				{
				setState(162);
				match(T__3);
				setState(167);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(163);
						match(WORD);
						setState(164);
						match(T__10);
						}
						} 
					}
					setState(169);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
				}
				setState(170);
				match(WORD);
				setState(171);
				match(T__4);
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FncallContext extends ParserRuleContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public FncallContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fncall; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterFncall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitFncall(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitFncall(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FncallContext fncall() throws RecognitionException {
		FncallContext _localctx = new FncallContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_fncall);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(174);
			expression(0);
			setState(175);
			match(T__1);
			setState(181);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(176);
					expression(0);
					setState(177);
					match(T__10);
					}
					} 
				}
				setState(183);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
			}
			setState(185);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__3) | (1L << T__22) | (1L << SIZEOF) | (1L << TYPEOF) | (1L << NEW) | (1L << TIMES))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (MOD - 64)) | (1L << (LAMBDA - 64)) | (1L << (AMPERSAND - 64)) | (1L << (VARIABLE_PHRASE - 64)) | (1L << (WORD - 64)) | (1L << (RAW_VAR - 64)) | (1L << (REF - 64)) | (1L << (NUMBER - 64)) | (1L << (FLOAT - 64)) | (1L << (STR - 64)))) != 0)) {
				{
				setState(184);
				expression(0);
				}
			}

			setState(187);
			match(T__2);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FnContext extends ParserRuleContext {
		public TerminalNode FN() { return getToken(XanatharParser.FN, 0); }
		public TerminalNode WORD() { return getToken(XanatharParser.WORD, 0); }
		public TerminalNode AS_PHRASE() { return getToken(XanatharParser.AS_PHRASE, 0); }
		public TerminalNode PARAMS() { return getToken(XanatharParser.PARAMS, 0); }
		public List<PhraseContext> phrase() {
			return getRuleContexts(PhraseContext.class);
		}
		public PhraseContext phrase(int i) {
			return getRuleContext(PhraseContext.class,i);
		}
		public FnContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fn; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterFn(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitFn(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitFn(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FnContext fn() throws RecognitionException {
		FnContext _localctx = new FnContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_fn);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(189);
			match(FN);
			setState(190);
			match(WORD);
			setState(191);
			match(T__1);
			setState(193);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==PARAMS) {
				{
				setState(192);
				match(PARAMS);
				}
			}

			setState(195);
			match(T__2);
			setState(196);
			match(AS_PHRASE);
			setState(197);
			match(T__8);
			setState(201);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__1) | (1L << T__3) | (1L << T__6) | (1L << T__14) | (1L << T__22) | (1L << T__23) | (1L << DECLARE) | (1L << LET) | (1L << STATIC) | (1L << SEALED) | (1L << EXTENDS) | (1L << FN) | (1L << HOOK) | (1L << TEMPLATE) | (1L << OVERRIDE) | (1L << IF) | (1L << FOR) | (1L << WHILE) | (1L << RET) | (1L << MATCH) | (1L << BREAK) | (1L << SKIP_) | (1L << LOCK) | (1L << UNLOCK) | (1L << SIZEOF) | (1L << TYPEOF) | (1L << NEW) | (1L << TIMES))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (MOD - 64)) | (1L << (LAMBDA - 64)) | (1L << (AMPERSAND - 64)) | (1L << (VARIABLE_PHRASE - 64)) | (1L << (WORD - 64)) | (1L << (USE_STATEMENT - 64)) | (1L << (RAW_VAR - 64)) | (1L << (INTERFACE_ - 64)) | (1L << (VIRTUAL_FN - 64)) | (1L << (REF - 64)) | (1L << (NUMBER - 64)) | (1L << (FLOAT - 64)) | (1L << (STR - 64)))) != 0)) {
				{
				{
				setState(198);
				phrase();
				}
				}
				setState(203);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(204);
			match(T__9);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class HookContext extends ParserRuleContext {
		public TerminalNode HOOK() { return getToken(XanatharParser.HOOK, 0); }
		public TerminalNode OP() { return getToken(XanatharParser.OP, 0); }
		public TerminalNode PARAMS() { return getToken(XanatharParser.PARAMS, 0); }
		public TerminalNode AS_PHRASE() { return getToken(XanatharParser.AS_PHRASE, 0); }
		public List<PhraseContext> phrase() {
			return getRuleContexts(PhraseContext.class);
		}
		public PhraseContext phrase(int i) {
			return getRuleContext(PhraseContext.class,i);
		}
		public HookContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_hook; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterHook(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitHook(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitHook(this);
			else return visitor.visitChildren(this);
		}
	}

	public final HookContext hook() throws RecognitionException {
		HookContext _localctx = new HookContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_hook);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(206);
			match(HOOK);
			setState(207);
			match(OP);
			setState(208);
			match(T__1);
			setState(209);
			match(PARAMS);
			setState(210);
			match(T__2);
			setState(212);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==AS_PHRASE) {
				{
				setState(211);
				match(AS_PHRASE);
				}
			}

			setState(214);
			match(T__8);
			setState(218);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__1) | (1L << T__3) | (1L << T__6) | (1L << T__14) | (1L << T__22) | (1L << T__23) | (1L << DECLARE) | (1L << LET) | (1L << STATIC) | (1L << SEALED) | (1L << EXTENDS) | (1L << FN) | (1L << HOOK) | (1L << TEMPLATE) | (1L << OVERRIDE) | (1L << IF) | (1L << FOR) | (1L << WHILE) | (1L << RET) | (1L << MATCH) | (1L << BREAK) | (1L << SKIP_) | (1L << LOCK) | (1L << UNLOCK) | (1L << SIZEOF) | (1L << TYPEOF) | (1L << NEW) | (1L << TIMES))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (MOD - 64)) | (1L << (LAMBDA - 64)) | (1L << (AMPERSAND - 64)) | (1L << (VARIABLE_PHRASE - 64)) | (1L << (WORD - 64)) | (1L << (USE_STATEMENT - 64)) | (1L << (RAW_VAR - 64)) | (1L << (INTERFACE_ - 64)) | (1L << (VIRTUAL_FN - 64)) | (1L << (REF - 64)) | (1L << (NUMBER - 64)) | (1L << (FLOAT - 64)) | (1L << (STR - 64)))) != 0)) {
				{
				{
				setState(215);
				phrase();
				}
				}
				setState(220);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(221);
			match(T__9);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Template_Context extends ParserRuleContext {
		public TerminalNode TEMPLATE() { return getToken(XanatharParser.TEMPLATE, 0); }
		public TerminalNode LEFT_ANGLE() { return getToken(XanatharParser.LEFT_ANGLE, 0); }
		public TerminalNode CLASSES() { return getToken(XanatharParser.CLASSES, 0); }
		public TerminalNode RIGHT_ANGLE() { return getToken(XanatharParser.RIGHT_ANGLE, 0); }
		public TerminalNode WORD() { return getToken(XanatharParser.WORD, 0); }
		public TerminalNode PARAMS() { return getToken(XanatharParser.PARAMS, 0); }
		public TerminalNode AS_PHRASE() { return getToken(XanatharParser.AS_PHRASE, 0); }
		public List<PhraseContext> phrase() {
			return getRuleContexts(PhraseContext.class);
		}
		public PhraseContext phrase(int i) {
			return getRuleContext(PhraseContext.class,i);
		}
		public Template_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_template_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterTemplate_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitTemplate_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitTemplate_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Template_Context template_() throws RecognitionException {
		Template_Context _localctx = new Template_Context(_ctx, getState());
		enterRule(_localctx, 26, RULE_template_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(223);
			match(TEMPLATE);
			setState(224);
			match(LEFT_ANGLE);
			setState(225);
			match(CLASSES);
			setState(226);
			match(RIGHT_ANGLE);
			setState(227);
			match(WORD);
			setState(228);
			match(T__1);
			setState(229);
			match(PARAMS);
			setState(230);
			match(T__2);
			setState(231);
			match(AS_PHRASE);
			setState(232);
			match(T__8);
			setState(236);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__1) | (1L << T__3) | (1L << T__6) | (1L << T__14) | (1L << T__22) | (1L << T__23) | (1L << DECLARE) | (1L << LET) | (1L << STATIC) | (1L << SEALED) | (1L << EXTENDS) | (1L << FN) | (1L << HOOK) | (1L << TEMPLATE) | (1L << OVERRIDE) | (1L << IF) | (1L << FOR) | (1L << WHILE) | (1L << RET) | (1L << MATCH) | (1L << BREAK) | (1L << SKIP_) | (1L << LOCK) | (1L << UNLOCK) | (1L << SIZEOF) | (1L << TYPEOF) | (1L << NEW) | (1L << TIMES))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (MOD - 64)) | (1L << (LAMBDA - 64)) | (1L << (AMPERSAND - 64)) | (1L << (VARIABLE_PHRASE - 64)) | (1L << (WORD - 64)) | (1L << (USE_STATEMENT - 64)) | (1L << (RAW_VAR - 64)) | (1L << (INTERFACE_ - 64)) | (1L << (VIRTUAL_FN - 64)) | (1L << (REF - 64)) | (1L << (NUMBER - 64)) | (1L << (FLOAT - 64)) | (1L << (STR - 64)))) != 0)) {
				{
				{
				setState(233);
				phrase();
				}
				}
				setState(238);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(239);
			match(T__9);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OverrideContext extends ParserRuleContext {
		public TerminalNode OVERRIDE() { return getToken(XanatharParser.OVERRIDE, 0); }
		public TerminalNode OVERRIDE_NAME() { return getToken(XanatharParser.OVERRIDE_NAME, 0); }
		public TerminalNode LEFT_ANGLE() { return getToken(XanatharParser.LEFT_ANGLE, 0); }
		public Override_paramContext override_param() {
			return getRuleContext(Override_paramContext.class,0);
		}
		public TerminalNode RIGHT_ANGLE() { return getToken(XanatharParser.RIGHT_ANGLE, 0); }
		public List<PhraseContext> phrase() {
			return getRuleContexts(PhraseContext.class);
		}
		public PhraseContext phrase(int i) {
			return getRuleContext(PhraseContext.class,i);
		}
		public OverrideContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_override; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterOverride(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitOverride(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitOverride(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OverrideContext override() throws RecognitionException {
		OverrideContext _localctx = new OverrideContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_override);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(241);
			match(OVERRIDE);
			setState(242);
			match(T__11);
			setState(243);
			match(OVERRIDE_NAME);
			setState(244);
			match(LEFT_ANGLE);
			setState(245);
			override_param();
			setState(246);
			match(RIGHT_ANGLE);
			setState(247);
			match(T__8);
			setState(251);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__1) | (1L << T__3) | (1L << T__6) | (1L << T__14) | (1L << T__22) | (1L << T__23) | (1L << DECLARE) | (1L << LET) | (1L << STATIC) | (1L << SEALED) | (1L << EXTENDS) | (1L << FN) | (1L << HOOK) | (1L << TEMPLATE) | (1L << OVERRIDE) | (1L << IF) | (1L << FOR) | (1L << WHILE) | (1L << RET) | (1L << MATCH) | (1L << BREAK) | (1L << SKIP_) | (1L << LOCK) | (1L << UNLOCK) | (1L << SIZEOF) | (1L << TYPEOF) | (1L << NEW) | (1L << TIMES))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (MOD - 64)) | (1L << (LAMBDA - 64)) | (1L << (AMPERSAND - 64)) | (1L << (VARIABLE_PHRASE - 64)) | (1L << (WORD - 64)) | (1L << (USE_STATEMENT - 64)) | (1L << (RAW_VAR - 64)) | (1L << (INTERFACE_ - 64)) | (1L << (VIRTUAL_FN - 64)) | (1L << (REF - 64)) | (1L << (NUMBER - 64)) | (1L << (FLOAT - 64)) | (1L << (STR - 64)))) != 0)) {
				{
				{
				setState(248);
				phrase();
				}
				}
				setState(253);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(254);
			match(T__9);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Override_paramContext extends ParserRuleContext {
		public List<TerminalNode> VARIABLE_PHRASE() { return getTokens(XanatharParser.VARIABLE_PHRASE); }
		public TerminalNode VARIABLE_PHRASE(int i) {
			return getToken(XanatharParser.VARIABLE_PHRASE, i);
		}
		public Override_paramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_override_param; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterOverride_param(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitOverride_param(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitOverride_param(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Override_paramContext override_param() throws RecognitionException {
		Override_paramContext _localctx = new Override_paramContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_override_param);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(260);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,20,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(256);
					match(VARIABLE_PHRASE);
					setState(257);
					match(T__10);
					}
					} 
				}
				setState(262);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,20,_ctx);
			}
			setState(264);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==VARIABLE_PHRASE) {
				{
				setState(263);
				match(VARIABLE_PHRASE);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LambdaContext extends ParserRuleContext {
		public TerminalNode LAMBDA() { return getToken(XanatharParser.LAMBDA, 0); }
		public TerminalNode PARAMS() { return getToken(XanatharParser.PARAMS, 0); }
		public TerminalNode ARROW() { return getToken(XanatharParser.ARROW, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<PhraseContext> phrase() {
			return getRuleContexts(PhraseContext.class);
		}
		public PhraseContext phrase(int i) {
			return getRuleContext(PhraseContext.class,i);
		}
		public LambdaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lambda; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterLambda(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitLambda(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitLambda(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LambdaContext lambda() throws RecognitionException {
		LambdaContext _localctx = new LambdaContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_lambda);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(266);
			match(LAMBDA);
			setState(267);
			match(PARAMS);
			setState(268);
			match(ARROW);
			setState(278);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__1:
			case T__3:
			case T__22:
			case SIZEOF:
			case TYPEOF:
			case NEW:
			case TIMES:
			case MOD:
			case LAMBDA:
			case AMPERSAND:
			case VARIABLE_PHRASE:
			case WORD:
			case RAW_VAR:
			case REF:
			case NUMBER:
			case FLOAT:
			case STR:
				{
				setState(269);
				expression(0);
				}
				break;
			case T__8:
				{
				{
				setState(270);
				match(T__8);
				setState(274);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__1) | (1L << T__3) | (1L << T__6) | (1L << T__14) | (1L << T__22) | (1L << T__23) | (1L << DECLARE) | (1L << LET) | (1L << STATIC) | (1L << SEALED) | (1L << EXTENDS) | (1L << FN) | (1L << HOOK) | (1L << TEMPLATE) | (1L << OVERRIDE) | (1L << IF) | (1L << FOR) | (1L << WHILE) | (1L << RET) | (1L << MATCH) | (1L << BREAK) | (1L << SKIP_) | (1L << LOCK) | (1L << UNLOCK) | (1L << SIZEOF) | (1L << TYPEOF) | (1L << NEW) | (1L << TIMES))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (MOD - 64)) | (1L << (LAMBDA - 64)) | (1L << (AMPERSAND - 64)) | (1L << (VARIABLE_PHRASE - 64)) | (1L << (WORD - 64)) | (1L << (USE_STATEMENT - 64)) | (1L << (RAW_VAR - 64)) | (1L << (INTERFACE_ - 64)) | (1L << (VIRTUAL_FN - 64)) | (1L << (REF - 64)) | (1L << (NUMBER - 64)) | (1L << (FLOAT - 64)) | (1L << (STR - 64)))) != 0)) {
					{
					{
					setState(271);
					phrase();
					}
					}
					setState(276);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(277);
				match(T__9);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class If_Context extends ParserRuleContext {
		public TerminalNode IF() { return getToken(XanatharParser.IF, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<PhraseContext> phrase() {
			return getRuleContexts(PhraseContext.class);
		}
		public PhraseContext phrase(int i) {
			return getRuleContext(PhraseContext.class,i);
		}
		public Else_Context else_() {
			return getRuleContext(Else_Context.class,0);
		}
		public If_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_if_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterIf_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitIf_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitIf_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final If_Context if_() throws RecognitionException {
		If_Context _localctx = new If_Context(_ctx, getState());
		enterRule(_localctx, 34, RULE_if_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(280);
			match(IF);
			setState(281);
			match(T__1);
			{
			setState(282);
			expression(0);
			}
			setState(283);
			match(T__2);
			setState(284);
			match(T__8);
			setState(288);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__1) | (1L << T__3) | (1L << T__6) | (1L << T__14) | (1L << T__22) | (1L << T__23) | (1L << DECLARE) | (1L << LET) | (1L << STATIC) | (1L << SEALED) | (1L << EXTENDS) | (1L << FN) | (1L << HOOK) | (1L << TEMPLATE) | (1L << OVERRIDE) | (1L << IF) | (1L << FOR) | (1L << WHILE) | (1L << RET) | (1L << MATCH) | (1L << BREAK) | (1L << SKIP_) | (1L << LOCK) | (1L << UNLOCK) | (1L << SIZEOF) | (1L << TYPEOF) | (1L << NEW) | (1L << TIMES))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (MOD - 64)) | (1L << (LAMBDA - 64)) | (1L << (AMPERSAND - 64)) | (1L << (VARIABLE_PHRASE - 64)) | (1L << (WORD - 64)) | (1L << (USE_STATEMENT - 64)) | (1L << (RAW_VAR - 64)) | (1L << (INTERFACE_ - 64)) | (1L << (VIRTUAL_FN - 64)) | (1L << (REF - 64)) | (1L << (NUMBER - 64)) | (1L << (FLOAT - 64)) | (1L << (STR - 64)))) != 0)) {
				{
				{
				setState(285);
				phrase();
				}
				}
				setState(290);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(291);
			match(T__9);
			setState(293);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ELSE) {
				{
				setState(292);
				else_();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Else_Context extends ParserRuleContext {
		public TerminalNode ELSE() { return getToken(XanatharParser.ELSE, 0); }
		public List<PhraseContext> phrase() {
			return getRuleContexts(PhraseContext.class);
		}
		public PhraseContext phrase(int i) {
			return getRuleContext(PhraseContext.class,i);
		}
		public Else_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_else_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterElse_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitElse_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitElse_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Else_Context else_() throws RecognitionException {
		Else_Context _localctx = new Else_Context(_ctx, getState());
		enterRule(_localctx, 36, RULE_else_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(295);
			match(ELSE);
			setState(296);
			match(T__8);
			setState(300);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__1) | (1L << T__3) | (1L << T__6) | (1L << T__14) | (1L << T__22) | (1L << T__23) | (1L << DECLARE) | (1L << LET) | (1L << STATIC) | (1L << SEALED) | (1L << EXTENDS) | (1L << FN) | (1L << HOOK) | (1L << TEMPLATE) | (1L << OVERRIDE) | (1L << IF) | (1L << FOR) | (1L << WHILE) | (1L << RET) | (1L << MATCH) | (1L << BREAK) | (1L << SKIP_) | (1L << LOCK) | (1L << UNLOCK) | (1L << SIZEOF) | (1L << TYPEOF) | (1L << NEW) | (1L << TIMES))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (MOD - 64)) | (1L << (LAMBDA - 64)) | (1L << (AMPERSAND - 64)) | (1L << (VARIABLE_PHRASE - 64)) | (1L << (WORD - 64)) | (1L << (USE_STATEMENT - 64)) | (1L << (RAW_VAR - 64)) | (1L << (INTERFACE_ - 64)) | (1L << (VIRTUAL_FN - 64)) | (1L << (REF - 64)) | (1L << (NUMBER - 64)) | (1L << (FLOAT - 64)) | (1L << (STR - 64)))) != 0)) {
				{
				{
				setState(297);
				phrase();
				}
				}
				setState(302);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(303);
			match(T__9);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class For_Context extends ParserRuleContext {
		public TerminalNode FOR() { return getToken(XanatharParser.FOR, 0); }
		public List<PhraseContext> phrase() {
			return getRuleContexts(PhraseContext.class);
		}
		public PhraseContext phrase(int i) {
			return getRuleContext(PhraseContext.class,i);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public For_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_for_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterFor_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitFor_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitFor_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final For_Context for_() throws RecognitionException {
		For_Context _localctx = new For_Context(_ctx, getState());
		enterRule(_localctx, 38, RULE_for_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(305);
			match(FOR);
			setState(306);
			match(T__1);
			setState(307);
			phrase();
			setState(308);
			expression(0);
			setState(309);
			match(T__12);
			setState(310);
			phrase();
			setState(311);
			match(T__2);
			setState(312);
			match(T__8);
			setState(316);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__1) | (1L << T__3) | (1L << T__6) | (1L << T__14) | (1L << T__22) | (1L << T__23) | (1L << DECLARE) | (1L << LET) | (1L << STATIC) | (1L << SEALED) | (1L << EXTENDS) | (1L << FN) | (1L << HOOK) | (1L << TEMPLATE) | (1L << OVERRIDE) | (1L << IF) | (1L << FOR) | (1L << WHILE) | (1L << RET) | (1L << MATCH) | (1L << BREAK) | (1L << SKIP_) | (1L << LOCK) | (1L << UNLOCK) | (1L << SIZEOF) | (1L << TYPEOF) | (1L << NEW) | (1L << TIMES))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (MOD - 64)) | (1L << (LAMBDA - 64)) | (1L << (AMPERSAND - 64)) | (1L << (VARIABLE_PHRASE - 64)) | (1L << (WORD - 64)) | (1L << (USE_STATEMENT - 64)) | (1L << (RAW_VAR - 64)) | (1L << (INTERFACE_ - 64)) | (1L << (VIRTUAL_FN - 64)) | (1L << (REF - 64)) | (1L << (NUMBER - 64)) | (1L << (FLOAT - 64)) | (1L << (STR - 64)))) != 0)) {
				{
				{
				setState(313);
				phrase();
				}
				}
				setState(318);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(319);
			match(T__9);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class While_Context extends ParserRuleContext {
		public TerminalNode WHILE() { return getToken(XanatharParser.WHILE, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<PhraseContext> phrase() {
			return getRuleContexts(PhraseContext.class);
		}
		public PhraseContext phrase(int i) {
			return getRuleContext(PhraseContext.class,i);
		}
		public While_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_while_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterWhile_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitWhile_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitWhile_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final While_Context while_() throws RecognitionException {
		While_Context _localctx = new While_Context(_ctx, getState());
		enterRule(_localctx, 40, RULE_while_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(321);
			match(WHILE);
			setState(322);
			match(T__1);
			setState(323);
			expression(0);
			setState(324);
			match(T__2);
			setState(325);
			match(T__8);
			setState(329);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__1) | (1L << T__3) | (1L << T__6) | (1L << T__14) | (1L << T__22) | (1L << T__23) | (1L << DECLARE) | (1L << LET) | (1L << STATIC) | (1L << SEALED) | (1L << EXTENDS) | (1L << FN) | (1L << HOOK) | (1L << TEMPLATE) | (1L << OVERRIDE) | (1L << IF) | (1L << FOR) | (1L << WHILE) | (1L << RET) | (1L << MATCH) | (1L << BREAK) | (1L << SKIP_) | (1L << LOCK) | (1L << UNLOCK) | (1L << SIZEOF) | (1L << TYPEOF) | (1L << NEW) | (1L << TIMES))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (MOD - 64)) | (1L << (LAMBDA - 64)) | (1L << (AMPERSAND - 64)) | (1L << (VARIABLE_PHRASE - 64)) | (1L << (WORD - 64)) | (1L << (USE_STATEMENT - 64)) | (1L << (RAW_VAR - 64)) | (1L << (INTERFACE_ - 64)) | (1L << (VIRTUAL_FN - 64)) | (1L << (REF - 64)) | (1L << (NUMBER - 64)) | (1L << (FLOAT - 64)) | (1L << (STR - 64)))) != 0)) {
				{
				{
				setState(326);
				phrase();
				}
				}
				setState(331);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(332);
			match(T__9);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RetContext extends ParserRuleContext {
		public TerminalNode RET() { return getToken(XanatharParser.RET, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public RetContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ret; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterRet(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitRet(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitRet(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RetContext ret() throws RecognitionException {
		RetContext _localctx = new RetContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_ret);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(334);
			match(RET);
			setState(336);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__3) | (1L << T__22) | (1L << SIZEOF) | (1L << TYPEOF) | (1L << NEW) | (1L << TIMES))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (MOD - 64)) | (1L << (LAMBDA - 64)) | (1L << (AMPERSAND - 64)) | (1L << (VARIABLE_PHRASE - 64)) | (1L << (WORD - 64)) | (1L << (RAW_VAR - 64)) | (1L << (REF - 64)) | (1L << (NUMBER - 64)) | (1L << (FLOAT - 64)) | (1L << (STR - 64)))) != 0)) {
				{
				setState(335);
				expression(0);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Match_Context extends ParserRuleContext {
		public TerminalNode MATCH() { return getToken(XanatharParser.MATCH, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<Match_armContext> match_arm() {
			return getRuleContexts(Match_armContext.class);
		}
		public Match_armContext match_arm(int i) {
			return getRuleContext(Match_armContext.class,i);
		}
		public Default_armContext default_arm() {
			return getRuleContext(Default_armContext.class,0);
		}
		public Match_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_match_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterMatch_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitMatch_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitMatch_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Match_Context match_() throws RecognitionException {
		Match_Context _localctx = new Match_Context(_ctx, getState());
		enterRule(_localctx, 44, RULE_match_);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(338);
			match(MATCH);
			setState(339);
			match(T__1);
			setState(340);
			expression(0);
			setState(341);
			match(T__2);
			setState(342);
			match(T__8);
			setState(348);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,30,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(343);
					match_arm();
					setState(344);
					match(T__10);
					}
					} 
				}
				setState(350);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,30,_ctx);
			}
			setState(353);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__1:
			case T__3:
			case T__22:
			case SIZEOF:
			case TYPEOF:
			case NEW:
			case TIMES:
			case MOD:
			case LAMBDA:
			case AMPERSAND:
			case VARIABLE_PHRASE:
			case WORD:
			case RAW_VAR:
			case REF:
			case NUMBER:
			case FLOAT:
			case STR:
				{
				setState(351);
				match_arm();
				}
				break;
			case DEFAULT:
				{
				setState(352);
				default_arm();
				}
				break;
			case T__9:
			case T__10:
				break;
			default:
				break;
			}
			setState(356);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__10) {
				{
				setState(355);
				match(T__10);
				}
			}

			setState(358);
			match(T__9);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Match_armContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public PhraseContext phrase() {
			return getRuleContext(PhraseContext.class,0);
		}
		public Match_armContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_match_arm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterMatch_arm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitMatch_arm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitMatch_arm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Match_armContext match_arm() throws RecognitionException {
		Match_armContext _localctx = new Match_armContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_match_arm);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(360);
			expression(0);
			setState(361);
			match(T__13);
			setState(362);
			phrase();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Default_armContext extends ParserRuleContext {
		public TerminalNode DEFAULT() { return getToken(XanatharParser.DEFAULT, 0); }
		public PhraseContext phrase() {
			return getRuleContext(PhraseContext.class,0);
		}
		public Default_armContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_default_arm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterDefault_arm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitDefault_arm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitDefault_arm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Default_armContext default_arm() throws RecognitionException {
		Default_armContext _localctx = new Default_armContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_default_arm);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(364);
			match(DEFAULT);
			setState(365);
			match(T__13);
			setState(366);
			phrase();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PtrContext extends ParserRuleContext {
		public TerminalNode AMPERSAND() { return getToken(XanatharParser.AMPERSAND, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public PtrContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ptr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterPtr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitPtr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitPtr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PtrContext ptr() throws RecognitionException {
		PtrContext _localctx = new PtrContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_ptr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(368);
			match(AMPERSAND);
			setState(369);
			expression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DerefContext extends ParserRuleContext {
		public TerminalNode TIMES() { return getToken(XanatharParser.TIMES, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public DerefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_deref; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterDeref(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitDeref(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitDeref(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DerefContext deref() throws RecognitionException {
		DerefContext _localctx = new DerefContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_deref);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(371);
			match(TIMES);
			setState(372);
			expression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class New_Context extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode AS() { return getToken(XanatharParser.AS, 0); }
		public FncallContext fncall() {
			return getRuleContext(FncallContext.class,0);
		}
		public New_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_new_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterNew_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitNew_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitNew_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final New_Context new_() throws RecognitionException {
		New_Context _localctx = new New_Context(_ctx, getState());
		enterRule(_localctx, 54, RULE_new_);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(374);
			match(T__14);
			setState(375);
			expression(0);
			setState(376);
			match(AS);
			setState(377);
			fncall();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LockContext extends ParserRuleContext {
		public TerminalNode LOCK() { return getToken(XanatharParser.LOCK, 0); }
		public TerminalNode VARIABLE_PHRASE() { return getToken(XanatharParser.VARIABLE_PHRASE, 0); }
		public LockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lock; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterLock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitLock(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitLock(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LockContext lock() throws RecognitionException {
		LockContext _localctx = new LockContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_lock);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(379);
			match(LOCK);
			setState(380);
			match(VARIABLE_PHRASE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UnlockContext extends ParserRuleContext {
		public TerminalNode UNLOCK() { return getToken(XanatharParser.UNLOCK, 0); }
		public TerminalNode VARIABLE_PHRASE() { return getToken(XanatharParser.VARIABLE_PHRASE, 0); }
		public UnlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unlock; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterUnlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitUnlock(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitUnlock(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UnlockContext unlock() throws RecognitionException {
		UnlockContext _localctx = new UnlockContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_unlock);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(382);
			match(UNLOCK);
			setState(383);
			match(VARIABLE_PHRASE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IncContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public IncContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_inc; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterInc(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitInc(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitInc(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IncContext inc() throws RecognitionException {
		IncContext _localctx = new IncContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_inc);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(385);
			expression(0);
			setState(386);
			match(T__15);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DecContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public DecContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dec; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterDec(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitDec(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitDec(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DecContext dec() throws RecognitionException {
		DecContext _localctx = new DecContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_dec);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(388);
			expression(0);
			setState(389);
			match(T__16);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RegContext extends ParserRuleContext {
		public TerminalNode MOD() { return getToken(XanatharParser.MOD, 0); }
		public TerminalNode WORD() { return getToken(XanatharParser.WORD, 0); }
		public RegContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_reg; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterReg(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitReg(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitReg(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RegContext reg() throws RecognitionException {
		RegContext _localctx = new RegContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_reg);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(391);
			match(MOD);
			setState(392);
			match(WORD);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Reg_setContext extends ParserRuleContext {
		public RegContext reg() {
			return getRuleContext(RegContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Reg_setContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_reg_set; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterReg_set(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitReg_set(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitReg_set(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Reg_setContext reg_set() throws RecognitionException {
		Reg_setContext _localctx = new Reg_setContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_reg_set);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(394);
			reg();
			setState(395);
			match(T__17);
			setState(396);
			expression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
	 
		public ExpressionContext() { }
		public void copyFrom(ExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class BinaryNotContext extends ExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public BinaryNotContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterBinaryNot(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitBinaryNot(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitBinaryNot(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LambdaCreationContext extends ExpressionContext {
		public TerminalNode LAMBDA() { return getToken(XanatharParser.LAMBDA, 0); }
		public TerminalNode PARAMS() { return getToken(XanatharParser.PARAMS, 0); }
		public TerminalNode ARROW() { return getToken(XanatharParser.ARROW, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<PhraseContext> phrase() {
			return getRuleContexts(PhraseContext.class);
		}
		public PhraseContext phrase(int i) {
			return getRuleContext(PhraseContext.class,i);
		}
		public LambdaCreationContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterLambdaCreation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitLambdaCreation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitLambdaCreation(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LessOrEqualContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode GEQ() { return getToken(XanatharParser.GEQ, 0); }
		public LessOrEqualContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterLessOrEqual(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitLessOrEqual(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitLessOrEqual(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MultiplicationContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode TIMES() { return getToken(XanatharParser.TIMES, 0); }
		public MultiplicationContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterMultiplication(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitMultiplication(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitMultiplication(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BinaryAndContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode AMPERSAND() { return getToken(XanatharParser.AMPERSAND, 0); }
		public BinaryAndContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterBinaryAnd(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitBinaryAnd(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitBinaryAnd(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BarewordContext extends ExpressionContext {
		public TerminalNode WORD() { return getToken(XanatharParser.WORD, 0); }
		public BarewordContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterBareword(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitBareword(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitBareword(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CallSealedClassContext extends ExpressionContext {
		public List<TerminalNode> WORD() { return getTokens(XanatharParser.WORD); }
		public TerminalNode WORD(int i) {
			return getToken(XanatharParser.WORD, i);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public CallSealedClassContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterCallSealedClass(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitCallSealedClass(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitCallSealedClass(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BinaryXorContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode CARET() { return getToken(XanatharParser.CARET, 0); }
		public BinaryXorContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterBinaryXor(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitBinaryXor(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitBinaryXor(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class GetSizeOfContext extends ExpressionContext {
		public TerminalNode SIZEOF() { return getToken(XanatharParser.SIZEOF, 0); }
		public TerminalNode TYPESPEC() { return getToken(XanatharParser.TYPESPEC, 0); }
		public TerminalNode WORD() { return getToken(XanatharParser.WORD, 0); }
		public GetSizeOfContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterGetSizeOf(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitGetSizeOf(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitGetSizeOf(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PointerContext extends ExpressionContext {
		public PtrContext ptr() {
			return getRuleContext(PtrContext.class,0);
		}
		public PointerContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterPointer(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitPointer(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitPointer(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IsContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode IS() { return getToken(XanatharParser.IS, 0); }
		public IsContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterIs(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitIs(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitIs(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class StringContext extends ExpressionContext {
		public TerminalNode STR() { return getToken(XanatharParser.STR, 0); }
		public StringContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterString(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitString(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitString(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ArrayIndexContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public ArrayIndexContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterArrayIndex(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitArrayIndex(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitArrayIndex(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NewCallContext extends ExpressionContext {
		public TerminalNode NEW() { return getToken(XanatharParser.NEW, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode AS() { return getToken(XanatharParser.AS, 0); }
		public NewCallContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterNewCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitNewCall(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitNewCall(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EqualContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public EqualContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterEqual(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitEqual(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitEqual(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IncrementContext extends ExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public IncrementContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterIncrement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitIncrement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitIncrement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class GetRegisterContext extends ExpressionContext {
		public RegContext reg() {
			return getRuleContext(RegContext.class,0);
		}
		public GetRegisterContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterGetRegister(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitGetRegister(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitGetRegister(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BinaryOrContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode PIPE() { return getToken(XanatharParser.PIPE, 0); }
		public BinaryOrContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterBinaryOr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitBinaryOr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitBinaryOr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DivisionContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode DIV() { return getToken(XanatharParser.DIV, 0); }
		public DivisionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterDivision(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitDivision(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitDivision(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FunctionCallContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public FunctionCallContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterFunctionCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitFunctionCall(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitFunctionCall(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LessContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode LEFT_ANGLE() { return getToken(XanatharParser.LEFT_ANGLE, 0); }
		public LessContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterLess(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitLess(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitLess(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ParenthesesContext extends ExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ParenthesesContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterParentheses(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitParentheses(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitParentheses(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DereferenceContext extends ExpressionContext {
		public DerefContext deref() {
			return getRuleContext(DerefContext.class,0);
		}
		public DereferenceContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterDereference(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitDereference(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitDereference(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class GenericCallContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode LEFT_ANGLE() { return getToken(XanatharParser.LEFT_ANGLE, 0); }
		public TerminalNode GP() { return getToken(XanatharParser.GP, 0); }
		public TerminalNode RIGHT_ANGLE() { return getToken(XanatharParser.RIGHT_ANGLE, 0); }
		public GenericCallContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterGenericCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitGenericCall(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitGenericCall(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class GetTypeOfContext extends ExpressionContext {
		public TerminalNode TYPEOF() { return getToken(XanatharParser.TYPEOF, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public GetTypeOfContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterGetTypeOf(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitGetTypeOf(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitGetTypeOf(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AdditionContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode PLUS() { return getToken(XanatharParser.PLUS, 0); }
		public AdditionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterAddition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitAddition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitAddition(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NotEqualContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public NotEqualContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterNotEqual(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitNotEqual(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitNotEqual(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DecrementContext extends ExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public DecrementContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterDecrement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitDecrement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitDecrement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ReferenceContext extends ExpressionContext {
		public TerminalNode REF() { return getToken(XanatharParser.REF, 0); }
		public ReferenceContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterReference(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitReference(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitReference(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ModuloContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode MOD() { return getToken(XanatharParser.MOD, 0); }
		public ModuloContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterModulo(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitModulo(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitModulo(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RawVariableContext extends ExpressionContext {
		public TerminalNode RAW_VAR() { return getToken(XanatharParser.RAW_VAR, 0); }
		public RawVariableContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterRawVariable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitRawVariable(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitRawVariable(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VariablePhraseContext extends ExpressionContext {
		public TerminalNode VARIABLE_PHRASE() { return getToken(XanatharParser.VARIABLE_PHRASE, 0); }
		public VariablePhraseContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterVariablePhrase(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitVariablePhrase(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitVariablePhrase(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class GreaterOrEqualContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode LEQ() { return getToken(XanatharParser.LEQ, 0); }
		public GreaterOrEqualContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterGreaterOrEqual(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitGreaterOrEqual(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitGreaterOrEqual(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RangeContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public RangeContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterRange(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitRange(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitRange(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NumberConstContext extends ExpressionContext {
		public TerminalNode NUMBER() { return getToken(XanatharParser.NUMBER, 0); }
		public NumberConstContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterNumberConst(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitNumberConst(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitNumberConst(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FloatConstContext extends ExpressionContext {
		public TerminalNode FLOAT() { return getToken(XanatharParser.FLOAT, 0); }
		public FloatConstContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterFloatConst(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitFloatConst(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitFloatConst(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RotateRightContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode ROR() { return getToken(XanatharParser.ROR, 0); }
		public RotateRightContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterRotateRight(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitRotateRight(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitRotateRight(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CastingContext extends ExpressionContext {
		public CastContext cast() {
			return getRuleContext(CastContext.class,0);
		}
		public CastingContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterCasting(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitCasting(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitCasting(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SubtractionContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode MINUS() { return getToken(XanatharParser.MINUS, 0); }
		public SubtractionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterSubtraction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitSubtraction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitSubtraction(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class GreaterContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode RIGHT_ANGLE() { return getToken(XanatharParser.RIGHT_ANGLE, 0); }
		public GreaterContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterGreater(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitGreater(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitGreater(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SpecContext extends ExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<TerminalNode> DOT() { return getTokens(XanatharParser.DOT); }
		public TerminalNode DOT(int i) {
			return getToken(XanatharParser.DOT, i);
		}
		public List<TerminalNode> WORD() { return getTokens(XanatharParser.WORD); }
		public TerminalNode WORD(int i) {
			return getToken(XanatharParser.WORD, i);
		}
		public SpecContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterSpec(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitSpec(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitSpec(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RotateLeftContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode ROL() { return getToken(XanatharParser.ROL, 0); }
		public RotateLeftContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterRotateLeft(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitRotateLeft(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitRotateLeft(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		return expression(0);
	}

	private ExpressionContext expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExpressionContext _localctx = new ExpressionContext(_ctx, _parentState);
		ExpressionContext _prevctx = _localctx;
		int _startState = 68;
		enterRecursionRule(_localctx, 68, RULE_expression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(474);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,40,_ctx) ) {
			case 1:
				{
				_localctx = new BarewordContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(399);
				match(WORD);
				}
				break;
			case 2:
				{
				_localctx = new ParenthesesContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				{
				setState(400);
				match(T__3);
				setState(401);
				expression(0);
				setState(402);
				match(T__4);
				}
				}
				break;
			case 3:
				{
				_localctx = new GetRegisterContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(404);
				reg();
				}
				break;
			case 4:
				{
				_localctx = new GetSizeOfContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(405);
				match(SIZEOF);
				setState(406);
				match(T__11);
				setState(407);
				_la = _input.LA(1);
				if ( !(_la==WORD || _la==TYPESPEC) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 5:
				{
				_localctx = new GetTypeOfContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(408);
				match(TYPEOF);
				setState(409);
				match(T__11);
				setState(410);
				expression(36);
				}
				break;
			case 6:
				{
				_localctx = new ReferenceContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(411);
				match(REF);
				}
				break;
			case 7:
				{
				_localctx = new CastingContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(412);
				cast();
				}
				break;
			case 8:
				{
				_localctx = new PointerContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(413);
				ptr();
				}
				break;
			case 9:
				{
				_localctx = new DereferenceContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(414);
				deref();
				}
				break;
			case 10:
				{
				_localctx = new NumberConstContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(415);
				match(NUMBER);
				}
				break;
			case 11:
				{
				_localctx = new FloatConstContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(416);
				match(FLOAT);
				}
				break;
			case 12:
				{
				_localctx = new StringContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(417);
				match(STR);
				}
				break;
			case 13:
				{
				_localctx = new VariablePhraseContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(418);
				match(VARIABLE_PHRASE);
				}
				break;
			case 14:
				{
				_localctx = new RawVariableContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(419);
				match(RAW_VAR);
				}
				break;
			case 15:
				{
				_localctx = new LambdaCreationContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(420);
				match(LAMBDA);
				setState(421);
				match(PARAMS);
				setState(422);
				match(ARROW);
				setState(432);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__1:
				case T__3:
				case T__22:
				case SIZEOF:
				case TYPEOF:
				case NEW:
				case TIMES:
				case MOD:
				case LAMBDA:
				case AMPERSAND:
				case VARIABLE_PHRASE:
				case WORD:
				case RAW_VAR:
				case REF:
				case NUMBER:
				case FLOAT:
				case STR:
					{
					setState(423);
					expression(0);
					}
					break;
				case T__8:
					{
					{
					setState(424);
					match(T__8);
					setState(428);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__1) | (1L << T__3) | (1L << T__6) | (1L << T__14) | (1L << T__22) | (1L << T__23) | (1L << DECLARE) | (1L << LET) | (1L << STATIC) | (1L << SEALED) | (1L << EXTENDS) | (1L << FN) | (1L << HOOK) | (1L << TEMPLATE) | (1L << OVERRIDE) | (1L << IF) | (1L << FOR) | (1L << WHILE) | (1L << RET) | (1L << MATCH) | (1L << BREAK) | (1L << SKIP_) | (1L << LOCK) | (1L << UNLOCK) | (1L << SIZEOF) | (1L << TYPEOF) | (1L << NEW) | (1L << TIMES))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (MOD - 64)) | (1L << (LAMBDA - 64)) | (1L << (AMPERSAND - 64)) | (1L << (VARIABLE_PHRASE - 64)) | (1L << (WORD - 64)) | (1L << (USE_STATEMENT - 64)) | (1L << (RAW_VAR - 64)) | (1L << (INTERFACE_ - 64)) | (1L << (VIRTUAL_FN - 64)) | (1L << (REF - 64)) | (1L << (NUMBER - 64)) | (1L << (FLOAT - 64)) | (1L << (STR - 64)))) != 0)) {
						{
						{
						setState(425);
						phrase();
						}
						}
						setState(430);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					setState(431);
					match(T__9);
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			case 16:
				{
				_localctx = new CallSealedClassContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(436); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(434);
						match(WORD);
						setState(435);
						match(T__18);
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(438); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,35,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				setState(440);
				match(WORD);
				setState(441);
				match(T__1);
				setState(447);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,36,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(442);
						expression(0);
						setState(443);
						match(T__10);
						}
						} 
					}
					setState(449);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,36,_ctx);
				}
				setState(451);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__3) | (1L << T__22) | (1L << SIZEOF) | (1L << TYPEOF) | (1L << NEW) | (1L << TIMES))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (MOD - 64)) | (1L << (LAMBDA - 64)) | (1L << (AMPERSAND - 64)) | (1L << (VARIABLE_PHRASE - 64)) | (1L << (WORD - 64)) | (1L << (RAW_VAR - 64)) | (1L << (REF - 64)) | (1L << (NUMBER - 64)) | (1L << (FLOAT - 64)) | (1L << (STR - 64)))) != 0)) {
					{
					setState(450);
					expression(0);
					}
				}

				setState(453);
				match(T__2);
				}
				break;
			case 17:
				{
				_localctx = new BinaryNotContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(454);
				match(T__22);
				setState(455);
				expression(2);
				}
				break;
			case 18:
				{
				_localctx = new NewCallContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(456);
				match(NEW);
				setState(457);
				expression(0);
				setState(458);
				match(AS);
				setState(459);
				expression(0);
				setState(460);
				match(T__1);
				setState(466);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,38,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(461);
						expression(0);
						setState(462);
						match(T__10);
						}
						} 
					}
					setState(468);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,38,_ctx);
				}
				setState(470);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__3) | (1L << T__22) | (1L << SIZEOF) | (1L << TYPEOF) | (1L << NEW) | (1L << TIMES))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (MOD - 64)) | (1L << (LAMBDA - 64)) | (1L << (AMPERSAND - 64)) | (1L << (VARIABLE_PHRASE - 64)) | (1L << (WORD - 64)) | (1L << (RAW_VAR - 64)) | (1L << (REF - 64)) | (1L << (NUMBER - 64)) | (1L << (FLOAT - 64)) | (1L << (STR - 64)))) != 0)) {
					{
					setState(469);
					expression(0);
					}
				}

				setState(472);
				match(T__2);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(582);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,47,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(580);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,46,_ctx) ) {
					case 1:
						{
						_localctx = new AdditionContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(476);
						if (!(precpred(_ctx, 25))) throw new FailedPredicateException(this, "precpred(_ctx, 25)");
						setState(477);
						match(PLUS);
						setState(478);
						expression(26);
						}
						break;
					case 2:
						{
						_localctx = new SubtractionContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(479);
						if (!(precpred(_ctx, 24))) throw new FailedPredicateException(this, "precpred(_ctx, 24)");
						setState(480);
						match(MINUS);
						setState(481);
						expression(25);
						}
						break;
					case 3:
						{
						_localctx = new MultiplicationContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(482);
						if (!(precpred(_ctx, 23))) throw new FailedPredicateException(this, "precpred(_ctx, 23)");
						setState(483);
						match(TIMES);
						setState(484);
						expression(24);
						}
						break;
					case 4:
						{
						_localctx = new DivisionContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(485);
						if (!(precpred(_ctx, 22))) throw new FailedPredicateException(this, "precpred(_ctx, 22)");
						setState(486);
						match(DIV);
						setState(487);
						expression(23);
						}
						break;
					case 5:
						{
						_localctx = new BinaryAndContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(488);
						if (!(precpred(_ctx, 21))) throw new FailedPredicateException(this, "precpred(_ctx, 21)");
						setState(489);
						match(AMPERSAND);
						setState(490);
						expression(22);
						}
						break;
					case 6:
						{
						_localctx = new BinaryOrContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(491);
						if (!(precpred(_ctx, 20))) throw new FailedPredicateException(this, "precpred(_ctx, 20)");
						setState(492);
						match(PIPE);
						setState(493);
						expression(21);
						}
						break;
					case 7:
						{
						_localctx = new BinaryXorContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(494);
						if (!(precpred(_ctx, 19))) throw new FailedPredicateException(this, "precpred(_ctx, 19)");
						setState(495);
						match(CARET);
						setState(496);
						expression(20);
						}
						break;
					case 8:
						{
						_localctx = new ModuloContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(497);
						if (!(precpred(_ctx, 18))) throw new FailedPredicateException(this, "precpred(_ctx, 18)");
						setState(498);
						match(MOD);
						setState(499);
						expression(19);
						}
						break;
					case 9:
						{
						_localctx = new RangeContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(500);
						if (!(precpred(_ctx, 13))) throw new FailedPredicateException(this, "precpred(_ctx, 13)");
						setState(501);
						match(T__19);
						setState(502);
						expression(14);
						}
						break;
					case 10:
						{
						_localctx = new IsContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(503);
						if (!(precpred(_ctx, 12))) throw new FailedPredicateException(this, "precpred(_ctx, 12)");
						setState(504);
						match(IS);
						setState(505);
						expression(13);
						}
						break;
					case 11:
						{
						_localctx = new RotateLeftContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(506);
						if (!(precpred(_ctx, 11))) throw new FailedPredicateException(this, "precpred(_ctx, 11)");
						setState(507);
						match(ROL);
						setState(508);
						expression(12);
						}
						break;
					case 12:
						{
						_localctx = new RotateRightContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(509);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						setState(510);
						match(ROR);
						setState(511);
						expression(11);
						}
						break;
					case 13:
						{
						_localctx = new LessContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(512);
						if (!(precpred(_ctx, 9))) throw new FailedPredicateException(this, "precpred(_ctx, 9)");
						setState(513);
						match(LEFT_ANGLE);
						setState(514);
						expression(10);
						}
						break;
					case 14:
						{
						_localctx = new GreaterContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(515);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(516);
						match(RIGHT_ANGLE);
						setState(517);
						expression(9);
						}
						break;
					case 15:
						{
						_localctx = new LessOrEqualContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(518);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(519);
						match(GEQ);
						setState(520);
						expression(8);
						}
						break;
					case 16:
						{
						_localctx = new GreaterOrEqualContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(521);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(522);
						match(LEQ);
						setState(523);
						expression(7);
						}
						break;
					case 17:
						{
						_localctx = new NotEqualContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(524);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(525);
						match(T__20);
						setState(526);
						expression(6);
						}
						break;
					case 18:
						{
						_localctx = new EqualContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(527);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(528);
						match(T__21);
						setState(529);
						expression(5);
						}
						break;
					case 19:
						{
						_localctx = new IncrementContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(530);
						if (!(precpred(_ctx, 39))) throw new FailedPredicateException(this, "precpred(_ctx, 39)");
						setState(531);
						match(T__15);
						}
						break;
					case 20:
						{
						_localctx = new DecrementContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(532);
						if (!(precpred(_ctx, 38))) throw new FailedPredicateException(this, "precpred(_ctx, 38)");
						setState(533);
						match(T__16);
						}
						break;
					case 21:
						{
						_localctx = new ArrayIndexContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(534);
						if (!(precpred(_ctx, 32))) throw new FailedPredicateException(this, "precpred(_ctx, 32)");
						setState(535);
						match(T__3);
						setState(536);
						expression(0);
						setState(537);
						match(T__4);
						}
						break;
					case 22:
						{
						_localctx = new FunctionCallContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(539);
						if (!(precpred(_ctx, 17))) throw new FailedPredicateException(this, "precpred(_ctx, 17)");
						setState(540);
						match(T__1);
						setState(546);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,41,_ctx);
						while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
							if ( _alt==1 ) {
								{
								{
								setState(541);
								expression(0);
								setState(542);
								match(T__10);
								}
								} 
							}
							setState(548);
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,41,_ctx);
						}
						setState(550);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__3) | (1L << T__22) | (1L << SIZEOF) | (1L << TYPEOF) | (1L << NEW) | (1L << TIMES))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (MOD - 64)) | (1L << (LAMBDA - 64)) | (1L << (AMPERSAND - 64)) | (1L << (VARIABLE_PHRASE - 64)) | (1L << (WORD - 64)) | (1L << (RAW_VAR - 64)) | (1L << (REF - 64)) | (1L << (NUMBER - 64)) | (1L << (FLOAT - 64)) | (1L << (STR - 64)))) != 0)) {
							{
							setState(549);
							expression(0);
							}
						}

						setState(552);
						match(T__2);
						}
						break;
					case 23:
						{
						_localctx = new GenericCallContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(553);
						if (!(precpred(_ctx, 16))) throw new FailedPredicateException(this, "precpred(_ctx, 16)");
						setState(554);
						match(LEFT_ANGLE);
						setState(555);
						match(GP);
						setState(556);
						match(RIGHT_ANGLE);
						setState(557);
						match(T__1);
						setState(563);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,43,_ctx);
						while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
							if ( _alt==1 ) {
								{
								{
								setState(558);
								expression(0);
								setState(559);
								match(T__10);
								}
								} 
							}
							setState(565);
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,43,_ctx);
						}
						setState(567);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__3) | (1L << T__22) | (1L << SIZEOF) | (1L << TYPEOF) | (1L << NEW) | (1L << TIMES))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (MOD - 64)) | (1L << (LAMBDA - 64)) | (1L << (AMPERSAND - 64)) | (1L << (VARIABLE_PHRASE - 64)) | (1L << (WORD - 64)) | (1L << (RAW_VAR - 64)) | (1L << (REF - 64)) | (1L << (NUMBER - 64)) | (1L << (FLOAT - 64)) | (1L << (STR - 64)))) != 0)) {
							{
							setState(566);
							expression(0);
							}
						}

						setState(569);
						match(T__2);
						}
						break;
					case 24:
						{
						_localctx = new SpecContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(570);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(571);
						match(DOT);
						setState(576);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,45,_ctx);
						while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
							if ( _alt==1 ) {
								{
								{
								setState(572);
								match(WORD);
								setState(573);
								match(DOT);
								}
								} 
							}
							setState(578);
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,45,_ctx);
						}
						setState(579);
						match(WORD);
						}
						break;
					}
					} 
				}
				setState(584);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,47,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class PhraseContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Reg_setContext reg_set() {
			return getRuleContext(Reg_setContext.class,0);
		}
		public PrepContext prep() {
			return getRuleContext(PrepContext.class,0);
		}
		public IncContext inc() {
			return getRuleContext(IncContext.class,0);
		}
		public DecContext dec() {
			return getRuleContext(DecContext.class,0);
		}
		public Asm_Context asm_() {
			return getRuleContext(Asm_Context.class,0);
		}
		public TerminalNode USE_STATEMENT() { return getToken(XanatharParser.USE_STATEMENT, 0); }
		public Declare_statementContext declare_statement() {
			return getRuleContext(Declare_statementContext.class,0);
		}
		public Template_Context template_() {
			return getRuleContext(Template_Context.class,0);
		}
		public LambdaContext lambda() {
			return getRuleContext(LambdaContext.class,0);
		}
		public RetContext ret() {
			return getRuleContext(RetContext.class,0);
		}
		public LockContext lock() {
			return getRuleContext(LockContext.class,0);
		}
		public UnlockContext unlock() {
			return getRuleContext(UnlockContext.class,0);
		}
		public Variable_settingContext variable_setting() {
			return getRuleContext(Variable_settingContext.class,0);
		}
		public LetContext let() {
			return getRuleContext(LetContext.class,0);
		}
		public TerminalNode BREAK() { return getToken(XanatharParser.BREAK, 0); }
		public TerminalNode SKIP_() { return getToken(XanatharParser.SKIP_, 0); }
		public New_Context new_() {
			return getRuleContext(New_Context.class,0);
		}
		public If_Context if_() {
			return getRuleContext(If_Context.class,0);
		}
		public FnContext fn() {
			return getRuleContext(FnContext.class,0);
		}
		public HookContext hook() {
			return getRuleContext(HookContext.class,0);
		}
		public TerminalNode VIRTUAL_FN() { return getToken(XanatharParser.VIRTUAL_FN, 0); }
		public Match_Context match_() {
			return getRuleContext(Match_Context.class,0);
		}
		public While_Context while_() {
			return getRuleContext(While_Context.class,0);
		}
		public For_Context for_() {
			return getRuleContext(For_Context.class,0);
		}
		public Class_Context class_() {
			return getRuleContext(Class_Context.class,0);
		}
		public TerminalNode INTERFACE_() { return getToken(XanatharParser.INTERFACE_, 0); }
		public OverrideContext override() {
			return getRuleContext(OverrideContext.class,0);
		}
		public Extends_Context extends_() {
			return getRuleContext(Extends_Context.class,0);
		}
		public PhraseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_phrase; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterPhrase(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitPhrase(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitPhrase(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PhraseContext phrase() throws RecognitionException {
		PhraseContext _localctx = new PhraseContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_phrase);
		try {
			setState(619);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__0:
			case T__1:
			case T__3:
			case T__14:
			case T__22:
			case T__23:
			case DECLARE:
			case LET:
			case TEMPLATE:
			case RET:
			case BREAK:
			case SKIP_:
			case LOCK:
			case UNLOCK:
			case SIZEOF:
			case TYPEOF:
			case NEW:
			case TIMES:
			case MOD:
			case LAMBDA:
			case AMPERSAND:
			case VARIABLE_PHRASE:
			case WORD:
			case USE_STATEMENT:
			case RAW_VAR:
			case REF:
			case NUMBER:
			case FLOAT:
			case STR:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(603);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,48,_ctx) ) {
				case 1:
					{
					setState(585);
					expression(0);
					}
					break;
				case 2:
					{
					setState(586);
					reg_set();
					}
					break;
				case 3:
					{
					setState(587);
					prep();
					}
					break;
				case 4:
					{
					setState(588);
					inc();
					}
					break;
				case 5:
					{
					setState(589);
					dec();
					}
					break;
				case 6:
					{
					setState(590);
					asm_();
					}
					break;
				case 7:
					{
					setState(591);
					match(USE_STATEMENT);
					}
					break;
				case 8:
					{
					setState(592);
					declare_statement();
					}
					break;
				case 9:
					{
					setState(593);
					template_();
					}
					break;
				case 10:
					{
					setState(594);
					lambda();
					}
					break;
				case 11:
					{
					setState(595);
					ret();
					}
					break;
				case 12:
					{
					setState(596);
					lock();
					}
					break;
				case 13:
					{
					setState(597);
					unlock();
					}
					break;
				case 14:
					{
					setState(598);
					variable_setting();
					}
					break;
				case 15:
					{
					setState(599);
					let();
					}
					break;
				case 16:
					{
					setState(600);
					match(BREAK);
					}
					break;
				case 17:
					{
					setState(601);
					match(SKIP_);
					}
					break;
				case 18:
					{
					setState(602);
					new_();
					}
					break;
				}
				setState(605);
				match(T__12);
				}
				}
				break;
			case T__6:
			case STATIC:
			case SEALED:
			case EXTENDS:
			case FN:
			case HOOK:
			case OVERRIDE:
			case IF:
			case FOR:
			case WHILE:
			case MATCH:
			case INTERFACE_:
			case VIRTUAL_FN:
				enterOuterAlt(_localctx, 2);
				{
				setState(617);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case IF:
					{
					setState(606);
					if_();
					}
					break;
				case FN:
					{
					setState(607);
					fn();
					}
					break;
				case HOOK:
					{
					setState(608);
					hook();
					}
					break;
				case VIRTUAL_FN:
					{
					setState(609);
					match(VIRTUAL_FN);
					}
					break;
				case MATCH:
					{
					setState(610);
					match_();
					}
					break;
				case WHILE:
					{
					setState(611);
					while_();
					}
					break;
				case FOR:
					{
					setState(612);
					for_();
					}
					break;
				case T__6:
				case STATIC:
				case SEALED:
					{
					setState(613);
					class_();
					}
					break;
				case INTERFACE_:
					{
					setState(614);
					match(INTERFACE_);
					}
					break;
				case OVERRIDE:
					{
					setState(615);
					override();
					}
					break;
				case EXTENDS:
					{
					setState(616);
					extends_();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StartContext extends ParserRuleContext {
		public List<PhraseContext> phrase() {
			return getRuleContexts(PhraseContext.class);
		}
		public PhraseContext phrase(int i) {
			return getRuleContext(PhraseContext.class,i);
		}
		public StartContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_start; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterStart(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitStart(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitStart(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StartContext start() throws RecognitionException {
		StartContext _localctx = new StartContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_start);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(624);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__1) | (1L << T__3) | (1L << T__6) | (1L << T__14) | (1L << T__22) | (1L << T__23) | (1L << DECLARE) | (1L << LET) | (1L << STATIC) | (1L << SEALED) | (1L << EXTENDS) | (1L << FN) | (1L << HOOK) | (1L << TEMPLATE) | (1L << OVERRIDE) | (1L << IF) | (1L << FOR) | (1L << WHILE) | (1L << RET) | (1L << MATCH) | (1L << BREAK) | (1L << SKIP_) | (1L << LOCK) | (1L << UNLOCK) | (1L << SIZEOF) | (1L << TYPEOF) | (1L << NEW) | (1L << TIMES))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (MOD - 64)) | (1L << (LAMBDA - 64)) | (1L << (AMPERSAND - 64)) | (1L << (VARIABLE_PHRASE - 64)) | (1L << (WORD - 64)) | (1L << (USE_STATEMENT - 64)) | (1L << (RAW_VAR - 64)) | (1L << (INTERFACE_ - 64)) | (1L << (VIRTUAL_FN - 64)) | (1L << (REF - 64)) | (1L << (NUMBER - 64)) | (1L << (FLOAT - 64)) | (1L << (STR - 64)))) != 0)) {
				{
				{
				setState(621);
				phrase();
				}
				}
				setState(626);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Asm_Context extends ParserRuleContext {
		public Asm_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_asm_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).enterAsm_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XanatharListener ) ((XanatharListener)listener).exitAsm_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof XanatharVisitor ) return ((XanatharVisitor<? extends T>)visitor).visitAsm_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Asm_Context asm_() throws RecognitionException {
		Asm_Context _localctx = new Asm_Context(_ctx, getState());
		enterRule(_localctx, 74, RULE_asm_);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(627);
			match(T__23);
			setState(629); 
			_errHandler.sync(this);
			_alt = 1+1;
			do {
				switch (_alt) {
				case 1+1:
					{
					{
					setState(628);
					matchWildcard();
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(631); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,52,_ctx);
			} while ( _alt!=1 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
			setState(633);
			match(T__24);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 34:
			return expression_sempred((ExpressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expression_sempred(ExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 25);
		case 1:
			return precpred(_ctx, 24);
		case 2:
			return precpred(_ctx, 23);
		case 3:
			return precpred(_ctx, 22);
		case 4:
			return precpred(_ctx, 21);
		case 5:
			return precpred(_ctx, 20);
		case 6:
			return precpred(_ctx, 19);
		case 7:
			return precpred(_ctx, 18);
		case 8:
			return precpred(_ctx, 13);
		case 9:
			return precpred(_ctx, 12);
		case 10:
			return precpred(_ctx, 11);
		case 11:
			return precpred(_ctx, 10);
		case 12:
			return precpred(_ctx, 9);
		case 13:
			return precpred(_ctx, 8);
		case 14:
			return precpred(_ctx, 7);
		case 15:
			return precpred(_ctx, 6);
		case 16:
			return precpred(_ctx, 5);
		case 17:
			return precpred(_ctx, 4);
		case 18:
			return precpred(_ctx, 39);
		case 19:
			return precpred(_ctx, 38);
		case 20:
			return precpred(_ctx, 32);
		case 21:
			return precpred(_ctx, 17);
		case 22:
			return precpred(_ctx, 16);
		case 23:
			return precpred(_ctx, 3);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3c\u027e\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\3\2\3\2\6\2Q\n\2\r\2\16\2"+
		"R\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4c\n\4\3\5"+
		"\3\5\3\5\3\5\3\5\5\5j\n\5\3\5\3\5\3\5\5\5o\n\5\3\6\3\6\3\6\3\6\3\6\3\7"+
		"\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7\177\n\7\3\7\3\7\7\7\u0083\n\7\f\7"+
		"\16\7\u0086\13\7\3\7\3\7\3\b\7\b\u008b\n\b\f\b\16\b\u008e\13\b\3\t\3\t"+
		"\3\t\3\t\7\t\u0094\n\t\f\t\16\t\u0097\13\t\3\t\3\t\3\n\3\n\7\n\u009d\n"+
		"\n\f\n\16\n\u00a0\13\n\3\n\5\n\u00a3\n\n\3\13\3\13\3\13\7\13\u00a8\n\13"+
		"\f\13\16\13\u00ab\13\13\3\13\3\13\5\13\u00af\n\13\3\f\3\f\3\f\3\f\3\f"+
		"\7\f\u00b6\n\f\f\f\16\f\u00b9\13\f\3\f\5\f\u00bc\n\f\3\f\3\f\3\r\3\r\3"+
		"\r\3\r\5\r\u00c4\n\r\3\r\3\r\3\r\3\r\7\r\u00ca\n\r\f\r\16\r\u00cd\13\r"+
		"\3\r\3\r\3\16\3\16\3\16\3\16\3\16\3\16\5\16\u00d7\n\16\3\16\3\16\7\16"+
		"\u00db\n\16\f\16\16\16\u00de\13\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17"+
		"\3\17\3\17\3\17\3\17\3\17\3\17\7\17\u00ed\n\17\f\17\16\17\u00f0\13\17"+
		"\3\17\3\17\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\7\20\u00fc\n\20\f\20"+
		"\16\20\u00ff\13\20\3\20\3\20\3\21\3\21\7\21\u0105\n\21\f\21\16\21\u0108"+
		"\13\21\3\21\5\21\u010b\n\21\3\22\3\22\3\22\3\22\3\22\3\22\7\22\u0113\n"+
		"\22\f\22\16\22\u0116\13\22\3\22\5\22\u0119\n\22\3\23\3\23\3\23\3\23\3"+
		"\23\3\23\7\23\u0121\n\23\f\23\16\23\u0124\13\23\3\23\3\23\5\23\u0128\n"+
		"\23\3\24\3\24\3\24\7\24\u012d\n\24\f\24\16\24\u0130\13\24\3\24\3\24\3"+
		"\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\7\25\u013d\n\25\f\25\16\25"+
		"\u0140\13\25\3\25\3\25\3\26\3\26\3\26\3\26\3\26\3\26\7\26\u014a\n\26\f"+
		"\26\16\26\u014d\13\26\3\26\3\26\3\27\3\27\5\27\u0153\n\27\3\30\3\30\3"+
		"\30\3\30\3\30\3\30\3\30\3\30\7\30\u015d\n\30\f\30\16\30\u0160\13\30\3"+
		"\30\3\30\5\30\u0164\n\30\3\30\5\30\u0167\n\30\3\30\3\30\3\31\3\31\3\31"+
		"\3\31\3\32\3\32\3\32\3\32\3\33\3\33\3\33\3\34\3\34\3\34\3\35\3\35\3\35"+
		"\3\35\3\35\3\36\3\36\3\36\3\37\3\37\3\37\3 \3 \3 \3!\3!\3!\3\"\3\"\3\""+
		"\3#\3#\3#\3#\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$"+
		"\3$\3$\3$\3$\3$\3$\3$\3$\3$\7$\u01ad\n$\f$\16$\u01b0\13$\3$\5$\u01b3\n"+
		"$\3$\3$\6$\u01b7\n$\r$\16$\u01b8\3$\3$\3$\3$\3$\7$\u01c0\n$\f$\16$\u01c3"+
		"\13$\3$\5$\u01c6\n$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\7$\u01d3\n$\f$\16"+
		"$\u01d6\13$\3$\5$\u01d9\n$\3$\3$\5$\u01dd\n$\3$\3$\3$\3$\3$\3$\3$\3$\3"+
		"$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3"+
		"$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3"+
		"$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\7$\u0223\n$\f$\16$\u0226\13$"+
		"\3$\5$\u0229\n$\3$\3$\3$\3$\3$\3$\3$\3$\3$\7$\u0234\n$\f$\16$\u0237\13"+
		"$\3$\5$\u023a\n$\3$\3$\3$\3$\3$\7$\u0241\n$\f$\16$\u0244\13$\3$\7$\u0247"+
		"\n$\f$\16$\u024a\13$\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3"+
		"%\3%\5%\u025e\n%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\5%\u026c\n%\5%\u026e"+
		"\n%\3&\7&\u0271\n&\f&\16&\u0274\13&\3\'\3\'\6\'\u0278\n\'\r\'\16\'\u0279"+
		"\3\'\3\'\3\'\4R\u0279\3F(\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&"+
		"(*,.\60\62\64\668:<>@BDFHJL\2\5\4\2OO^^\3\2#$\4\2QQTT\2\u02cc\2N\3\2\2"+
		"\2\4V\3\2\2\2\6[\3\2\2\2\bd\3\2\2\2\np\3\2\2\2\fu\3\2\2\2\16\u008c\3\2"+
		"\2\2\20\u008f\3\2\2\2\22\u00a2\3\2\2\2\24\u00ae\3\2\2\2\26\u00b0\3\2\2"+
		"\2\30\u00bf\3\2\2\2\32\u00d0\3\2\2\2\34\u00e1\3\2\2\2\36\u00f3\3\2\2\2"+
		" \u0106\3\2\2\2\"\u010c\3\2\2\2$\u011a\3\2\2\2&\u0129\3\2\2\2(\u0133\3"+
		"\2\2\2*\u0143\3\2\2\2,\u0150\3\2\2\2.\u0154\3\2\2\2\60\u016a\3\2\2\2\62"+
		"\u016e\3\2\2\2\64\u0172\3\2\2\2\66\u0175\3\2\2\28\u0178\3\2\2\2:\u017d"+
		"\3\2\2\2<\u0180\3\2\2\2>\u0183\3\2\2\2@\u0186\3\2\2\2B\u0189\3\2\2\2D"+
		"\u018c\3\2\2\2F\u01dc\3\2\2\2H\u026d\3\2\2\2J\u0272\3\2\2\2L\u0275\3\2"+
		"\2\2NP\7\3\2\2OQ\13\2\2\2PO\3\2\2\2QR\3\2\2\2RS\3\2\2\2RP\3\2\2\2ST\3"+
		"\2\2\2TU\7\3\2\2U\3\3\2\2\2VW\7\4\2\2WX\5F$\2XY\7P\2\2YZ\7\5\2\2Z\5\3"+
		"\2\2\2[\\\7\37\2\2\\]\7O\2\2]b\7P\2\2^_\7\6\2\2_`\5F$\2`a\7\7\2\2ac\3"+
		"\2\2\2b^\3\2\2\2bc\3\2\2\2c\7\3\2\2\2di\t\2\2\2ef\7\6\2\2fg\5F$\2gh\7"+
		"\7\2\2hj\3\2\2\2ie\3\2\2\2ij\3\2\2\2jk\3\2\2\2kn\7\b\2\2lo\5F$\2mo\5\26"+
		"\f\2nl\3\2\2\2nm\3\2\2\2o\t\3\2\2\2pq\7\"\2\2qr\7O\2\2rs\7\b\2\2st\5F"+
		"$\2t\13\3\2\2\2uv\5\16\b\2vw\7\t\2\2wx\7Q\2\2xy\7F\2\2yz\5\22\n\2z{\7"+
		"G\2\2{~\5\24\13\2|}\7\n\2\2}\177\5\24\13\2~|\3\2\2\2~\177\3\2\2\2\177"+
		"\u0080\3\2\2\2\u0080\u0084\7\13\2\2\u0081\u0083\5H%\2\u0082\u0081\3\2"+
		"\2\2\u0083\u0086\3\2\2\2\u0084\u0082\3\2\2\2\u0084\u0085\3\2\2\2\u0085"+
		"\u0087\3\2\2\2\u0086\u0084\3\2\2\2\u0087\u0088\7\f\2\2\u0088\r\3\2\2\2"+
		"\u0089\u008b\t\3\2\2\u008a\u0089\3\2\2\2\u008b\u008e\3\2\2\2\u008c\u008a"+
		"\3\2\2\2\u008c\u008d\3\2\2\2\u008d\17\3\2\2\2\u008e\u008c\3\2\2\2\u008f"+
		"\u0090\7%\2\2\u0090\u0091\7Q\2\2\u0091\u0095\7\13\2\2\u0092\u0094\5H%"+
		"\2\u0093\u0092\3\2\2\2\u0094\u0097\3\2\2\2\u0095\u0093\3\2\2\2\u0095\u0096"+
		"\3\2\2\2\u0096\u0098\3\2\2\2\u0097\u0095\3\2\2\2\u0098\u0099\7\f\2\2\u0099"+
		"\21\3\2\2\2\u009a\u009b\7W\2\2\u009b\u009d\7\r\2\2\u009c\u009a\3\2\2\2"+
		"\u009d\u00a0\3\2\2\2\u009e\u009c\3\2\2\2\u009e\u009f\3\2\2\2\u009f\u00a1"+
		"\3\2\2\2\u00a0\u009e\3\2\2\2\u00a1\u00a3\7W\2\2\u00a2\u009e\3\2\2\2\u00a2"+
		"\u00a3\3\2\2\2\u00a3\23\3\2\2\2\u00a4\u00a9\7\6\2\2\u00a5\u00a6\7Q\2\2"+
		"\u00a6\u00a8\7\r\2\2\u00a7\u00a5\3\2\2\2\u00a8\u00ab\3\2\2\2\u00a9\u00a7"+
		"\3\2\2\2\u00a9\u00aa\3\2\2\2\u00aa\u00ac\3\2\2\2\u00ab\u00a9\3\2\2\2\u00ac"+
		"\u00ad\7Q\2\2\u00ad\u00af\7\7\2\2\u00ae\u00a4\3\2\2\2\u00ae\u00af\3\2"+
		"\2\2\u00af\25\3\2\2\2\u00b0\u00b1\5F$\2\u00b1\u00b7\7\4\2\2\u00b2\u00b3"+
		"\5F$\2\u00b3\u00b4\7\r\2\2\u00b4\u00b6\3\2\2\2\u00b5\u00b2\3\2\2\2\u00b6"+
		"\u00b9\3\2\2\2\u00b7\u00b5\3\2\2\2\u00b7\u00b8\3\2\2\2\u00b8\u00bb\3\2"+
		"\2\2\u00b9\u00b7\3\2\2\2\u00ba\u00bc\5F$\2\u00bb\u00ba\3\2\2\2\u00bb\u00bc"+
		"\3\2\2\2\u00bc\u00bd\3\2\2\2\u00bd\u00be\7\5\2\2\u00be\27\3\2\2\2\u00bf"+
		"\u00c0\7\'\2\2\u00c0\u00c1\7Q\2\2\u00c1\u00c3\7\4\2\2\u00c2\u00c4\7N\2"+
		"\2\u00c3\u00c2\3\2\2\2\u00c3\u00c4\3\2\2\2\u00c4\u00c5\3\2\2\2\u00c5\u00c6"+
		"\7\5\2\2\u00c6\u00c7\7P\2\2\u00c7\u00cb\7\13\2\2\u00c8\u00ca\5H%\2\u00c9"+
		"\u00c8\3\2\2\2\u00ca\u00cd\3\2\2\2\u00cb\u00c9\3\2\2\2\u00cb\u00cc\3\2"+
		"\2\2\u00cc\u00ce\3\2\2\2\u00cd\u00cb\3\2\2\2\u00ce\u00cf\7\f\2\2\u00cf"+
		"\31\3\2\2\2\u00d0\u00d1\7(\2\2\u00d1\u00d2\7Y\2\2\u00d2\u00d3\7\4\2\2"+
		"\u00d3\u00d4\7N\2\2\u00d4\u00d6\7\5\2\2\u00d5\u00d7\7P\2\2\u00d6\u00d5"+
		"\3\2\2\2\u00d6\u00d7\3\2\2\2\u00d7\u00d8\3\2\2\2\u00d8\u00dc\7\13\2\2"+
		"\u00d9\u00db\5H%\2\u00da\u00d9\3\2\2\2\u00db\u00de\3\2\2\2\u00dc\u00da"+
		"\3\2\2\2\u00dc\u00dd\3\2\2\2\u00dd\u00df\3\2\2\2\u00de\u00dc\3\2\2\2\u00df"+
		"\u00e0\7\f\2\2\u00e0\33\3\2\2\2\u00e1\u00e2\7)\2\2\u00e2\u00e3\7F\2\2"+
		"\u00e3\u00e4\7Z\2\2\u00e4\u00e5\7G\2\2\u00e5\u00e6\7Q\2\2\u00e6\u00e7"+
		"\7\4\2\2\u00e7\u00e8\7N\2\2\u00e8\u00e9\7\5\2\2\u00e9\u00ea\7P\2\2\u00ea"+
		"\u00ee\7\13\2\2\u00eb\u00ed\5H%\2\u00ec\u00eb\3\2\2\2\u00ed\u00f0\3\2"+
		"\2\2\u00ee\u00ec\3\2\2\2\u00ee\u00ef\3\2\2\2\u00ef\u00f1\3\2\2\2\u00f0"+
		"\u00ee\3\2\2\2\u00f1\u00f2\7\f\2\2\u00f2\35\3\2\2\2\u00f3\u00f4\7-\2\2"+
		"\u00f4\u00f5\7\16\2\2\u00f5\u00f6\7]\2\2\u00f6\u00f7\7F\2\2\u00f7\u00f8"+
		"\5 \21\2\u00f8\u00f9\7G\2\2\u00f9\u00fd\7\13\2\2\u00fa\u00fc\5H%\2\u00fb"+
		"\u00fa\3\2\2\2\u00fc\u00ff\3\2\2\2\u00fd\u00fb\3\2\2\2\u00fd\u00fe\3\2"+
		"\2\2\u00fe\u0100\3\2\2\2\u00ff\u00fd\3\2\2\2\u0100\u0101\7\f\2\2\u0101"+
		"\37\3\2\2\2\u0102\u0103\7O\2\2\u0103\u0105\7\r\2\2\u0104\u0102\3\2\2\2"+
		"\u0105\u0108\3\2\2\2\u0106\u0104\3\2\2\2\u0106\u0107\3\2\2\2\u0107\u010a"+
		"\3\2\2\2\u0108\u0106\3\2\2\2\u0109\u010b\7O\2\2\u010a\u0109\3\2\2\2\u010a"+
		"\u010b\3\2\2\2\u010b!\3\2\2\2\u010c\u010d\7E\2\2\u010d\u010e\7N\2\2\u010e"+
		"\u0118\7=\2\2\u010f\u0119\5F$\2\u0110\u0114\7\13\2\2\u0111\u0113\5H%\2"+
		"\u0112\u0111\3\2\2\2\u0113\u0116\3\2\2\2\u0114\u0112\3\2\2\2\u0114\u0115"+
		"\3\2\2\2\u0115\u0117\3\2\2\2\u0116\u0114\3\2\2\2\u0117\u0119\7\f\2\2\u0118"+
		"\u010f\3\2\2\2\u0118\u0110\3\2\2\2\u0119#\3\2\2\2\u011a\u011b\7.\2\2\u011b"+
		"\u011c\7\4\2\2\u011c\u011d\5F$\2\u011d\u011e\7\5\2\2\u011e\u0122\7\13"+
		"\2\2\u011f\u0121\5H%\2\u0120\u011f\3\2\2\2\u0121\u0124\3\2\2\2\u0122\u0120"+
		"\3\2\2\2\u0122\u0123\3\2\2\2\u0123\u0125\3\2\2\2\u0124\u0122\3\2\2\2\u0125"+
		"\u0127\7\f\2\2\u0126\u0128\5&\24\2\u0127\u0126\3\2\2\2\u0127\u0128\3\2"+
		"\2\2\u0128%\3\2\2\2\u0129\u012a\7/\2\2\u012a\u012e\7\13\2\2\u012b\u012d"+
		"\5H%\2\u012c\u012b\3\2\2\2\u012d\u0130\3\2\2\2\u012e\u012c\3\2\2\2\u012e"+
		"\u012f\3\2\2\2\u012f\u0131\3\2\2\2\u0130\u012e\3\2\2\2\u0131\u0132\7\f"+
		"\2\2\u0132\'\3\2\2\2\u0133\u0134\7\61\2\2\u0134\u0135\7\4\2\2\u0135\u0136"+
		"\5H%\2\u0136\u0137\5F$\2\u0137\u0138\7\17\2\2\u0138\u0139\5H%\2\u0139"+
		"\u013a\7\5\2\2\u013a\u013e\7\13\2\2\u013b\u013d\5H%\2\u013c\u013b\3\2"+
		"\2\2\u013d\u0140\3\2\2\2\u013e\u013c\3\2\2\2\u013e\u013f\3\2\2\2\u013f"+
		"\u0141\3\2\2\2\u0140\u013e\3\2\2\2\u0141\u0142\7\f\2\2\u0142)\3\2\2\2"+
		"\u0143\u0144\7\62\2\2\u0144\u0145\7\4\2\2\u0145\u0146\5F$\2\u0146\u0147"+
		"\7\5\2\2\u0147\u014b\7\13\2\2\u0148\u014a\5H%\2\u0149\u0148\3\2\2\2\u014a"+
		"\u014d\3\2\2\2\u014b\u0149\3\2\2\2\u014b\u014c\3\2\2\2\u014c\u014e\3\2"+
		"\2\2\u014d\u014b\3\2\2\2\u014e\u014f\7\f\2\2\u014f+\3\2\2\2\u0150\u0152"+
		"\7\63\2\2\u0151\u0153\5F$\2\u0152\u0151\3\2\2\2\u0152\u0153\3\2\2\2\u0153"+
		"-\3\2\2\2\u0154\u0155\7\64\2\2\u0155\u0156\7\4\2\2\u0156\u0157\5F$\2\u0157"+
		"\u0158\7\5\2\2\u0158\u015e\7\13\2\2\u0159\u015a\5\60\31\2\u015a\u015b"+
		"\7\r\2\2\u015b\u015d\3\2\2\2\u015c\u0159\3\2\2\2\u015d\u0160\3\2\2\2\u015e"+
		"\u015c\3\2\2\2\u015e\u015f\3\2\2\2\u015f\u0163\3\2\2\2\u0160\u015e\3\2"+
		"\2\2\u0161\u0164\5\60\31\2\u0162\u0164\5\62\32\2\u0163\u0161\3\2\2\2\u0163"+
		"\u0162\3\2\2\2\u0163\u0164\3\2\2\2\u0164\u0166\3\2\2\2\u0165\u0167\7\r"+
		"\2\2\u0166\u0165\3\2\2\2\u0166\u0167\3\2\2\2\u0167\u0168\3\2\2\2\u0168"+
		"\u0169\7\f\2\2\u0169/\3\2\2\2\u016a\u016b\5F$\2\u016b\u016c\7\20\2\2\u016c"+
		"\u016d\5H%\2\u016d\61\3\2\2\2\u016e\u016f\7\65\2\2\u016f\u0170\7\20\2"+
		"\2\u0170\u0171\5H%\2\u0171\63\3\2\2\2\u0172\u0173\7K\2\2\u0173\u0174\5"+
		"F$\2\u0174\65\3\2\2\2\u0175\u0176\7@\2\2\u0176\u0177\5F$\2\u0177\67\3"+
		"\2\2\2\u0178\u0179\7\21\2\2\u0179\u017a\5F$\2\u017a\u017b\7,\2\2\u017b"+
		"\u017c\5\26\f\2\u017c9\3\2\2\2\u017d\u017e\78\2\2\u017e\u017f\7O\2\2\u017f"+
		";\3\2\2\2\u0180\u0181\79\2\2\u0181\u0182\7O\2\2\u0182=\3\2\2\2\u0183\u0184"+
		"\5F$\2\u0184\u0185\7\22\2\2\u0185?\3\2\2\2\u0186\u0187\5F$\2\u0187\u0188"+
		"\7\23\2\2\u0188A\3\2\2\2\u0189\u018a\7B\2\2\u018a\u018b\7Q\2\2\u018bC"+
		"\3\2\2\2\u018c\u018d\5B\"\2\u018d\u018e\7\24\2\2\u018e\u018f\5F$\2\u018f"+
		"E\3\2\2\2\u0190\u0191\b$\1\2\u0191\u01dd\7Q\2\2\u0192\u0193\7\6\2\2\u0193"+
		"\u0194\5F$\2\u0194\u0195\7\7\2\2\u0195\u01dd\3\2\2\2\u0196\u01dd\5B\""+
		"\2\u0197\u0198\7:\2\2\u0198\u0199\7\16\2\2\u0199\u01dd\t\4\2\2\u019a\u019b"+
		"\7;\2\2\u019b\u019c\7\16\2\2\u019c\u01dd\5F$&\u019d\u01dd\7^\2\2\u019e"+
		"\u01dd\5\4\3\2\u019f\u01dd\5\64\33\2\u01a0\u01dd\5\66\34\2\u01a1\u01dd"+
		"\7_\2\2\u01a2\u01dd\7`\2\2\u01a3\u01dd\7a\2\2\u01a4\u01dd\7O\2\2\u01a5"+
		"\u01dd\7S\2\2\u01a6\u01a7\7E\2\2\u01a7\u01a8\7N\2\2\u01a8\u01b2\7=\2\2"+
		"\u01a9\u01b3\5F$\2\u01aa\u01ae\7\13\2\2\u01ab\u01ad\5H%\2\u01ac\u01ab"+
		"\3\2\2\2\u01ad\u01b0\3\2\2\2\u01ae\u01ac\3\2\2\2\u01ae\u01af\3\2\2\2\u01af"+
		"\u01b1\3\2\2\2\u01b0\u01ae\3\2\2\2\u01b1\u01b3\7\f\2\2\u01b2\u01a9\3\2"+
		"\2\2\u01b2\u01aa\3\2\2\2\u01b3\u01dd\3\2\2\2\u01b4\u01b5\7Q\2\2\u01b5"+
		"\u01b7\7\25\2\2\u01b6\u01b4\3\2\2\2\u01b7\u01b8\3\2\2\2\u01b8\u01b6\3"+
		"\2\2\2\u01b8\u01b9\3\2\2\2\u01b9\u01ba\3\2\2\2\u01ba\u01bb\7Q\2\2\u01bb"+
		"\u01c1\7\4\2\2\u01bc\u01bd\5F$\2\u01bd\u01be\7\r\2\2\u01be\u01c0\3\2\2"+
		"\2\u01bf\u01bc\3\2\2\2\u01c0\u01c3\3\2\2\2\u01c1\u01bf\3\2\2\2\u01c1\u01c2"+
		"\3\2\2\2\u01c2\u01c5\3\2\2\2\u01c3\u01c1\3\2\2\2\u01c4\u01c6\5F$\2\u01c5"+
		"\u01c4\3\2\2\2\u01c5\u01c6\3\2\2\2\u01c6\u01c7\3\2\2\2\u01c7\u01dd\7\5"+
		"\2\2\u01c8\u01c9\7\31\2\2\u01c9\u01dd\5F$\4\u01ca\u01cb\7<\2\2\u01cb\u01cc"+
		"\5F$\2\u01cc\u01cd\7,\2\2\u01cd\u01ce\5F$\2\u01ce\u01d4\7\4\2\2\u01cf"+
		"\u01d0\5F$\2\u01d0\u01d1\7\r\2\2\u01d1\u01d3\3\2\2\2\u01d2\u01cf\3\2\2"+
		"\2\u01d3\u01d6\3\2\2\2\u01d4\u01d2\3\2\2\2\u01d4\u01d5\3\2\2\2\u01d5\u01d8"+
		"\3\2\2\2\u01d6\u01d4\3\2\2\2\u01d7\u01d9\5F$\2\u01d8\u01d7\3\2\2\2\u01d8"+
		"\u01d9\3\2\2\2\u01d9\u01da\3\2\2\2\u01da\u01db\7\5\2\2\u01db\u01dd\3\2"+
		"\2\2\u01dc\u0190\3\2\2\2\u01dc\u0192\3\2\2\2\u01dc\u0196\3\2\2\2\u01dc"+
		"\u0197\3\2\2\2\u01dc\u019a\3\2\2\2\u01dc\u019d\3\2\2\2\u01dc\u019e\3\2"+
		"\2\2\u01dc\u019f\3\2\2\2\u01dc\u01a0\3\2\2\2\u01dc\u01a1\3\2\2\2\u01dc"+
		"\u01a2\3\2\2\2\u01dc\u01a3\3\2\2\2\u01dc\u01a4\3\2\2\2\u01dc\u01a5\3\2"+
		"\2\2\u01dc\u01a6\3\2\2\2\u01dc\u01b6\3\2\2\2\u01dc\u01c8\3\2\2\2\u01dc"+
		"\u01ca\3\2\2\2\u01dd\u0248\3\2\2\2\u01de\u01df\f\33\2\2\u01df\u01e0\7"+
		">\2\2\u01e0\u0247\5F$\34\u01e1\u01e2\f\32\2\2\u01e2\u01e3\7?\2\2\u01e3"+
		"\u0247\5F$\33\u01e4\u01e5\f\31\2\2\u01e5\u01e6\7@\2\2\u01e6\u0247\5F$"+
		"\32\u01e7\u01e8\f\30\2\2\u01e8\u01e9\7A\2\2\u01e9\u0247\5F$\31\u01ea\u01eb"+
		"\f\27\2\2\u01eb\u01ec\7K\2\2\u01ec\u0247\5F$\30\u01ed\u01ee\f\26\2\2\u01ee"+
		"\u01ef\7L\2\2\u01ef\u0247\5F$\27\u01f0\u01f1\f\25\2\2\u01f1\u01f2\7M\2"+
		"\2\u01f2\u0247\5F$\26\u01f3\u01f4\f\24\2\2\u01f4\u01f5\7B\2\2\u01f5\u0247"+
		"\5F$\25\u01f6\u01f7\f\17\2\2\u01f7\u01f8\7\26\2\2\u01f8\u0247\5F$\20\u01f9"+
		"\u01fa\f\16\2\2\u01fa\u01fb\7\60\2\2\u01fb\u0247\5F$\17\u01fc\u01fd\f"+
		"\r\2\2\u01fd\u01fe\7C\2\2\u01fe\u0247\5F$\16\u01ff\u0200\f\f\2\2\u0200"+
		"\u0201\7D\2\2\u0201\u0247\5F$\r\u0202\u0203\f\13\2\2\u0203\u0204\7F\2"+
		"\2\u0204\u0247\5F$\f\u0205\u0206\f\n\2\2\u0206\u0207\7G\2\2\u0207\u0247"+
		"\5F$\13\u0208\u0209\f\t\2\2\u0209\u020a\7H\2\2\u020a\u0247\5F$\n\u020b"+
		"\u020c\f\b\2\2\u020c\u020d\7I\2\2\u020d\u0247\5F$\t\u020e\u020f\f\7\2"+
		"\2\u020f\u0210\7\27\2\2\u0210\u0247\5F$\b\u0211\u0212\f\6\2\2\u0212\u0213"+
		"\7\30\2\2\u0213\u0247\5F$\7\u0214\u0215\f)\2\2\u0215\u0247\7\22\2\2\u0216"+
		"\u0217\f(\2\2\u0217\u0247\7\23\2\2\u0218\u0219\f\"\2\2\u0219\u021a\7\6"+
		"\2\2\u021a\u021b\5F$\2\u021b\u021c\7\7\2\2\u021c\u0247\3\2\2\2\u021d\u021e"+
		"\f\23\2\2\u021e\u0224\7\4\2\2\u021f\u0220\5F$\2\u0220\u0221\7\r\2\2\u0221"+
		"\u0223\3\2\2\2\u0222\u021f\3\2\2\2\u0223\u0226\3\2\2\2\u0224\u0222\3\2"+
		"\2\2\u0224\u0225\3\2\2\2\u0225\u0228\3\2\2\2\u0226\u0224\3\2\2\2\u0227"+
		"\u0229\5F$\2\u0228\u0227\3\2\2\2\u0228\u0229\3\2\2\2\u0229\u022a\3\2\2"+
		"\2\u022a\u0247\7\5\2\2\u022b\u022c\f\22\2\2\u022c\u022d\7F\2\2\u022d\u022e"+
		"\7X\2\2\u022e\u022f\7G\2\2\u022f\u0235\7\4\2\2\u0230\u0231\5F$\2\u0231"+
		"\u0232\7\r\2\2\u0232\u0234\3\2\2\2\u0233\u0230\3\2\2\2\u0234\u0237\3\2"+
		"\2\2\u0235\u0233\3\2\2\2\u0235\u0236\3\2\2\2\u0236\u0239\3\2\2\2\u0237"+
		"\u0235\3\2\2\2\u0238\u023a\5F$\2\u0239\u0238\3\2\2\2\u0239\u023a\3\2\2"+
		"\2\u023a\u023b\3\2\2\2\u023b\u0247\7\5\2\2\u023c\u023d\f\5\2\2\u023d\u0242"+
		"\7J\2\2\u023e\u023f\7Q\2\2\u023f\u0241\7J\2\2\u0240\u023e\3\2\2\2\u0241"+
		"\u0244\3\2\2\2\u0242\u0240\3\2\2\2\u0242\u0243\3\2\2\2\u0243\u0245\3\2"+
		"\2\2\u0244\u0242\3\2\2\2\u0245\u0247\7Q\2\2\u0246\u01de\3\2\2\2\u0246"+
		"\u01e1\3\2\2\2\u0246\u01e4\3\2\2\2\u0246\u01e7\3\2\2\2\u0246\u01ea\3\2"+
		"\2\2\u0246\u01ed\3\2\2\2\u0246\u01f0\3\2\2\2\u0246\u01f3\3\2\2\2\u0246"+
		"\u01f6\3\2\2\2\u0246\u01f9\3\2\2\2\u0246\u01fc\3\2\2\2\u0246\u01ff\3\2"+
		"\2\2\u0246\u0202\3\2\2\2\u0246\u0205\3\2\2\2\u0246\u0208\3\2\2\2\u0246"+
		"\u020b\3\2\2\2\u0246\u020e\3\2\2\2\u0246\u0211\3\2\2\2\u0246\u0214\3\2"+
		"\2\2\u0246\u0216\3\2\2\2\u0246\u0218\3\2\2\2\u0246\u021d\3\2\2\2\u0246"+
		"\u022b\3\2\2\2\u0246\u023c\3\2\2\2\u0247\u024a\3\2\2\2\u0248\u0246\3\2"+
		"\2\2\u0248\u0249\3\2\2\2\u0249G\3\2\2\2\u024a\u0248\3\2\2\2\u024b\u025e"+
		"\5F$\2\u024c\u025e\5D#\2\u024d\u025e\5\2\2\2\u024e\u025e\5> \2\u024f\u025e"+
		"\5@!\2\u0250\u025e\5L\'\2\u0251\u025e\7R\2\2\u0252\u025e\5\6\4\2\u0253"+
		"\u025e\5\34\17\2\u0254\u025e\5\"\22\2\u0255\u025e\5,\27\2\u0256\u025e"+
		"\5:\36\2\u0257\u025e\5<\37\2\u0258\u025e\5\b\5\2\u0259\u025e\5\n\6\2\u025a"+
		"\u025e\7\66\2\2\u025b\u025e\7\67\2\2\u025c\u025e\58\35\2\u025d\u024b\3"+
		"\2\2\2\u025d\u024c\3\2\2\2\u025d\u024d\3\2\2\2\u025d\u024e\3\2\2\2\u025d"+
		"\u024f\3\2\2\2\u025d\u0250\3\2\2\2\u025d\u0251\3\2\2\2\u025d\u0252\3\2"+
		"\2\2\u025d\u0253\3\2\2\2\u025d\u0254\3\2\2\2\u025d\u0255\3\2\2\2\u025d"+
		"\u0256\3\2\2\2\u025d\u0257\3\2\2\2\u025d\u0258\3\2\2\2\u025d\u0259\3\2"+
		"\2\2\u025d\u025a\3\2\2\2\u025d\u025b\3\2\2\2\u025d\u025c\3\2\2\2\u025e"+
		"\u025f\3\2\2\2\u025f\u026e\7\17\2\2\u0260\u026c\5$\23\2\u0261\u026c\5"+
		"\30\r\2\u0262\u026c\5\32\16\2\u0263\u026c\7\\\2\2\u0264\u026c\5.\30\2"+
		"\u0265\u026c\5*\26\2\u0266\u026c\5(\25\2\u0267\u026c\5\f\7\2\u0268\u026c"+
		"\7U\2\2\u0269\u026c\5\36\20\2\u026a\u026c\5\20\t\2\u026b\u0260\3\2\2\2"+
		"\u026b\u0261\3\2\2\2\u026b\u0262\3\2\2\2\u026b\u0263\3\2\2\2\u026b\u0264"+
		"\3\2\2\2\u026b\u0265\3\2\2\2\u026b\u0266\3\2\2\2\u026b\u0267\3\2\2\2\u026b"+
		"\u0268\3\2\2\2\u026b\u0269\3\2\2\2\u026b\u026a\3\2\2\2\u026c\u026e\3\2"+
		"\2\2\u026d\u025d\3\2\2\2\u026d\u026b\3\2\2\2\u026eI\3\2\2\2\u026f\u0271"+
		"\5H%\2\u0270\u026f\3\2\2\2\u0271\u0274\3\2\2\2\u0272\u0270\3\2\2\2\u0272"+
		"\u0273\3\2\2\2\u0273K\3\2\2\2\u0274\u0272\3\2\2\2\u0275\u0277\7\32\2\2"+
		"\u0276\u0278\13\2\2\2\u0277\u0276\3\2\2\2\u0278\u0279\3\2\2\2\u0279\u027a"+
		"\3\2\2\2\u0279\u0277\3\2\2\2\u027a\u027b\3\2\2\2\u027b\u027c\7\33\2\2"+
		"\u027cM\3\2\2\2\67Rbin~\u0084\u008c\u0095\u009e\u00a2\u00a9\u00ae\u00b7"+
		"\u00bb\u00c3\u00cb\u00d6\u00dc\u00ee\u00fd\u0106\u010a\u0114\u0118\u0122"+
		"\u0127\u012e\u013e\u014b\u0152\u015e\u0163\u0166\u01ae\u01b2\u01b8\u01c1"+
		"\u01c5\u01d4\u01d8\u01dc\u0224\u0228\u0235\u0239\u0242\u0246\u0248\u025d"+
		"\u026b\u026d\u0272\u0279";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}